﻿using System.ComponentModel.DataAnnotations;

namespace ImoGest.Models
{
    public class Photo
    {
        public int PhotoID { get; set; }
        [StringLength(255)]
        public string PhotoName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }

        public int RealEstateID { get; set; }
        public virtual RealEstate RealEstate { get; set; }
    }
}