﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGest.Models
{
    public class RealEstate
    {
        public int ID { get; set; }
        [Display(Name = "Descrição do Imóvel"), Required, StringLength(120, MinimumLength = 8)]
        public string Description { get; set; }

        [Display(Name = "Localização")]
        public virtual Localization Localization { get; set; }

        [Display(Name = "Tipo de Imóvel")]
        public int TypologyID { get; set; }
        public virtual RealEstateTypology Typology { get; set; }

        [Display(Name = "Área"), Required]
        public double Area { get; set; }

        [Display(Name = "Fotografias")]
        public virtual ICollection<Photo> Photos { get; set; }

        [ForeignKey("Owner")]
        public string OwnerID { get; set; }
        public virtual ApplicationUser Owner { get; set; }
    }
}