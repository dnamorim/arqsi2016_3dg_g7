﻿namespace ImoGest.Models
{
    public class Sale : AdvertType
    {
        public override string Name
        {
            get
            {
                return "Venda";
            }
        }
    }
}