﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGest.Models
{
    public class RealEstateTypology
    {
        public int ID { get; set; }

        [Required, Display(Name = "Tipo"), StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [ForeignKey("Parent"), Display(Name = "Sub-Tipo de")]
        public int? ParentID { get; set; }
        public virtual RealEstateTypology Parent { get; set; }

        [NotMapped, Display(Name = "Descrição Completa")]
        public string FullDescription {
            get {
                return $"{Parent?.FullDescription} {this.Name}";
            }
        }
        
    }
}