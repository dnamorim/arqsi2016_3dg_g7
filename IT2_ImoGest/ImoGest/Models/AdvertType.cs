﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ImoGest.Models
{
    public abstract class AdvertType
    {
        public int ID { get; set; }
        public abstract string Name { get; }
    }
}