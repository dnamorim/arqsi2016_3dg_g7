﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGest.Models
{
    public class AlertParameter
    {
        public int ID { get; set; }
        [Required]
        public Field Field { get; set; }
        [Required]
        public Constraint Constraint { get; set; }
        [Required, StringLength(100, MinimumLength = 2)]
        public string Value { get; set; }

        [ForeignKey("Alert")]
        public int AlertID { get; set; }
        public virtual Alert Alert { get; set; }
    }

    public class AlertParameterField
    {
        [Column(Order = 0), Key]
        public Field Field { get; set; }
        [Column(Order = 1), Key]
        public Constraint Constraint { get; set; }
    }

    public enum Field
    {
        [Display(Name = "Tipo de Imóvel")]
        RealEstateTypology = 1,

        [Display(Name = "Tipo de Anúncio")]
        AdvertType = 2,

        [Display(Name = "Área")]
        Area = 3,

        [Display(Name = "Preço")]
        Price = 4,

    }

    public enum Constraint
    {
        [Display(Name = "Igual a (=)")]
        Equals = 1,

        [Display(Name = "Menor que (<)")]
        Less = 2,

        [Display(Name = "Maior que (>)")]
        More = 3
    }
}