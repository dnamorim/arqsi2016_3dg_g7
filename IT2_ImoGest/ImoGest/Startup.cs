﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImoGest.Startup))]
namespace ImoGest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
