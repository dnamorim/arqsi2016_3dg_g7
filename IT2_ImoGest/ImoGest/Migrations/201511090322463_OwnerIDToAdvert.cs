namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OwnerIDToAdvert : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Adverts", name: "Owner_Id", newName: "OwnerID");
            RenameIndex(table: "dbo.Adverts", name: "IX_Owner_Id", newName: "IX_OwnerID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Adverts", name: "IX_OwnerID", newName: "IX_Owner_Id");
            RenameColumn(table: "dbo.Adverts", name: "OwnerID", newName: "Owner_Id");
        }
    }
}
