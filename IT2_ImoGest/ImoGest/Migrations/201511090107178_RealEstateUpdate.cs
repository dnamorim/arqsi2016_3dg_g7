namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstateUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RealEstates", "Area", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RealEstates", "Area");
        }
    }
}
