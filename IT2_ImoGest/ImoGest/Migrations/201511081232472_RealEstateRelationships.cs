namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstateRelationships : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.RealEstates", name: "TypeID", newName: "Type_ID");
            RenameIndex(table: "dbo.RealEstates", name: "IX_TypeID", newName: "IX_Type_ID");
            AlterColumn("dbo.RealEstates", "Description", c => c.String(nullable: false));
            DropColumn("dbo.RealEstates", "LocalizationID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RealEstates", "LocalizationID", c => c.Int(nullable: false));
            AlterColumn("dbo.RealEstates", "Description", c => c.String());
            RenameIndex(table: "dbo.RealEstates", name: "IX_Type_ID", newName: "IX_TypeID");
            RenameColumn(table: "dbo.RealEstates", name: "Type_ID", newName: "TypeID");
        }
    }
}
