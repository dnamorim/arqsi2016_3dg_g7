namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlertParameterFieldsV2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.AlertParameterFields");
            AddColumn("dbo.AlertParameterFields", "Constraint", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.AlertParameterFields", new[] { "Field", "Constraint" });
            DropColumn("dbo.AlertParameterFields", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AlertParameterFields", "ID", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.AlertParameterFields");
            DropColumn("dbo.AlertParameterFields", "Constraint");
            AddPrimaryKey("dbo.AlertParameterFields", "ID");
        }
    }
}
