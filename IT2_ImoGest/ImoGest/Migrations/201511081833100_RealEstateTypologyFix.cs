namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstateTypologyFix : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.RealEstates", name: "TypeID", newName: "TypologyID");
            RenameIndex(table: "dbo.RealEstates", name: "IX_TypeID", newName: "IX_TypologyID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.RealEstates", name: "IX_TypologyID", newName: "IX_TypeID");
            RenameColumn(table: "dbo.RealEstates", name: "TypologyID", newName: "TypeID");
        }
    }
}
