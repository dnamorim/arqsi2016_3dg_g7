namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdvertKeys : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Adverts", name: "Property_ID", newName: "RealEstateID");
            RenameIndex(table: "dbo.Adverts", name: "IX_Property_ID", newName: "IX_RealEstateID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Adverts", name: "IX_RealEstateID", newName: "IX_Property_ID");
            RenameColumn(table: "dbo.Adverts", name: "RealEstateID", newName: "Property_ID");
        }
    }
}
