namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreationAndUpdateDatesAdvert : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Adverts", "DateCreation", c => c.DateTime(nullable: false));
            AddColumn("dbo.Adverts", "DateLastUpdate", c => c.DateTime(nullable: false));
            //DropColumn("dbo.Adverts", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Adverts", "Date", c => c.DateTime(nullable: false));
            DropColumn("dbo.Adverts", "DateLastUpdate");
            DropColumn("dbo.Adverts", "DateCreation");
        }
    }
}
