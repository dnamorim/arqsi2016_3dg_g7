namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdvertType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdvertTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AdvertTypes");
        }
    }
}
