namespace ImoGest.Migrations
{
    using Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<ImoGest.DataAccessLayer.ImoGestContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ImoGest.DataAccessLayer.ImoGestContext";
        }

        protected override void Seed(ImoGest.DataAccessLayer.ImoGestContext context)
        {
            #region Add Users and Roles
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            String[] roles = { "Admin", "Client" };
            foreach (var role in roles)
            {
                if (!roleManager.RoleExists(role))
                {
                    roleManager.Create(new IdentityRole(role));
                }
            }

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            String[] userEmails = { "admin@isep.ipp.pt", "client@isep.ipp.pt" };
            for (int i = 0; i < userEmails.Length; i++)
            {
                var user = new ApplicationUser { Email = userEmails[i], UserName = userEmails[i] };
                var userCreationResult = userManager.Create(user, "Arqsi2015.");
                if (userCreationResult.Succeeded)
                {
                    userManager.AddToRole(user.Id, roles[i]);
                }
            }
            #endregion

            #region Add RealEstateTypologies
            context.RealEstateTypologies.AddOrUpdate(o => o.ID, new RealEstateTypology { ID = 1, Name = "Apartamento" });
            context.RealEstateTypologies.AddOrUpdate(o => o.ID, new RealEstateTypology { ID = 2, Name = "Moradia" });
            context.RealEstateTypologies.AddOrUpdate(o => o.ID, new RealEstateTypology { ID = 3, Name = "T4", ParentID = 1 });
            #endregion

            #region Add AdvertTypes
            context.AdvertTypes.AddOrUpdate(o => o.ID, new Buy { ID = 1 });
            context.AdvertTypes.AddOrUpdate(o => o.ID, new Sale { ID = 2 });
            context.AdvertTypes.AddOrUpdate(o => o.ID, new Rent { ID = 3 });
            context.AdvertTypes.AddOrUpdate(o => o.ID, new Exchange { ID = 4 });
            #endregion

            #region Add AlertParameterFields
            context.AlertParameterFields.AddOrUpdate(
                new AlertParameterField
                {
                    Field = Field.RealEstateTypology,
                    Constraint = Constraint.Equals
                });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.AdvertType,
                   Constraint = Constraint.Equals
               });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.Area,
                   Constraint = Constraint.Equals
               });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.Area,
                   Constraint = Constraint.Less
               });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.Area,
                   Constraint = Constraint.More
               });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.Price,
                   Constraint = Constraint.Equals
               });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.Price,
                   Constraint = Constraint.Less
               });

            context.AlertParameterFields.AddOrUpdate(
               new AlertParameterField
               {
                   Field = Field.Price,
                   Constraint = Constraint.More
               });
            #endregion

        }
    }
}
