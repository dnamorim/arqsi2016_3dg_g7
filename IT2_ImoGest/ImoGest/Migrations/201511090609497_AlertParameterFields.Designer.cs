// <auto-generated />
namespace ImoGest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AlertParameterFields : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AlertParameterFields));
        
        string IMigrationMetadata.Id
        {
            get { return "201511090609497_AlertParameterFields"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
