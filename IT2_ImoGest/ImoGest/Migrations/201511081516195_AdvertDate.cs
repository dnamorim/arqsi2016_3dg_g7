namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdvertDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Adverts", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Adverts", "Date");
        }
    }
}
