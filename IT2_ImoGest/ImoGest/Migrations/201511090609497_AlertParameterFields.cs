namespace ImoGest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlertParameterFields : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AlertParameterFields",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Field = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AlertParameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        field = c.Int(nullable: false),
                        constraint = c.Int(nullable: false),
                        Value = c.String(),
                        AlertID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Alerts", t => t.AlertID, cascadeDelete: true)
                .Index(t => t.AlertID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AlertParameters", "AlertID", "dbo.Alerts");
            DropIndex("dbo.AlertParameters", new[] { "AlertID" });
            DropTable("dbo.AlertParameters");
            DropTable("dbo.AlertParameterFields");
        }
    }
}
