var arrayFacetas = new Array();
var arraySemanticaFacetas = new Array();

/* Inicializa objecto para fazer questões à API */
function createXMLHttpRequest() {
  var xmlHttpObj;

  try {
    xmlHttpObj = new XMLHttpRequest();
  } catch (e) {
    try {
      xmlHttpObj = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
      xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
    }
  }

  return xmlHttpObj;
}

/* inicializa o Widget */
function loadWidget(locationId) {
  loadScriptsAndLinks();
  var xmlHttpObject = createXMLHttpRequest();

  xmlHttpObject.open("GET", encodeURI("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php"), true);
  xmlHttpObject.onreadystatechange = function() {
    findAvailableFacetas(xmlHttpObject, locationId);
  }
  xmlHttpObject.send(null);
}

function loadScriptsAndLinks() {
  var head = document.getElementsByTagName("head")[0];

  var jquery = document.createElement("script");
  jquery.setAttribute("src", "/Scripts/jquery-2.1.4.min.js");

  var bootstrapJS = document.createElement("script");
  bootstrapJS.setAttribute("src", "/Scripts/bootstrap.min.js");

  var bsSliderJS = document.createElement("script");
  bsSliderJS.setAttribute("src", "/Scripts/bootstrap-slider.js");

  var bootstrapCSS = document.createElement("link");
  bootstrapCSS.setAttribute("rel", "stylesheet");
  bootstrapCSS.setAttribute("href", "/Content/bootstrap.min.css");

  var bsSliderCSS = document.createElement("link");
  bsSliderCSS.setAttribute("rel", "stylesheet");
  bsSliderCSS.setAttribute("href", "/Content/bootstrap-slider.css");

  head.appendChild(jquery);
  head.appendChild(bootstrapJS);
  head.appendChild(bsSliderJS);

  head.appendChild(bootstrapCSS);
  head.appendChild(bsSliderCSS);
}

/* Cria os filtros(select´s) e adiciona-os ao HTML */
function findAvailableFacetas(xmlHttpObject, locationId) {
  if (xmlHttpObject.readyState == 4 && xmlHttpObject.status == 200) {
    var facetas = xmlHttpObject.responseXML.getElementsByTagName("faceta");
    for (i = 0; i < facetas.length; i++) {
      arrayFacetas.push(facetas[i].childNodes[0].nodeValue);
      }

    createWidgetArea(locationId);
    createFacetas();
  } else {
    if (xmlHttpObject.readyState == 4) {
        var msg = document.createElement("div");
        msg.setAttribute("class", "alert alert-warning");
        msg.setAttribute("role","alert");
        var title = document.createElement("strong");
        title.appendChild(document.createTextNode("Something went wrong... "));
        msg.appendChild(title);
        msg.appendChild(document.createTextNode("Unable to connect to API Rest ImoWidget (DEI VPN down?)."))
        
        var loc = document.getElementById(locationId);
        loc.appendChild(document.createElement("br"));
        loc.appendChild(msg);
    }
  }
}

function createFacetas() {
  var newFaceta;

  for (i = 0; i < arrayFacetas.length; i++) {
    createFaceta(arrayFacetas[i], document.getElementById("filters"));
  }
}

function createWidgetArea(locationId) {
  var divColMd12 = document.createElement("div");
  divColMd12.setAttribute("class", "col-md-12");

  var divFilters = document.createElement("div");
  divFilters.setAttribute("id", "filters");
  divFilters.style.display = "inline-block";

  var divTable = document.createElement("div");
  divTable.setAttribute("class", "table-responsive");
  divTable.setAttribute("id", "imoveisSearchResultDiv");
  divTable.style.visibility = "hidden";
  divTable.style["max-width"] = "70%";
  divTable.style.display = "inline-block";

  var table = document.createElement("table");
  table.setAttribute("id", "imoveisSearchResultTable");
  table.setAttribute("class", "table");

  var trHeader = document.createElement("tr");

  var th;
  for (var i = 0; i < arrayFacetas.length; i++) {
    th = document.createElement("th");
    th.appendChild(document.createTextNode(arrayFacetas[i]));
    trHeader.appendChild(th);
  }
  var thead = document.createElement("thead");
  thead.appendChild(trHeader);

  table.appendChild(thead);
  table.appendChild(document.createElement("tbody"));

  divTable.appendChild(table);

  divColMd12.appendChild(divFilters);
  divColMd12.appendChild(divTable);

  var divRow = document.createElement("div");
  divRow.setAttribute("class", "row");
  divRow.appendChild(divColMd12);

  var divContainer = document.createElement("div");
  divContainer.setAttribute("class", "container");
  divContainer.appendChild(divRow);

  var divJumbotron = document.createElement("div");
  divJumbotron.setAttribute("class", "jumbotron");
  divJumbotron.appendChild(divContainer);

  if (locationId != null) {
    document.getElementById(locationId).appendChild(divJumbotron);
  } else {
      document.body.appendChild(divJumbotron);
  }
}

/* Cria um Select para uma dada Faceta (com todos os seus valores) */
function createFaceta(name, locationToAppend) {
  //var newFaceta = null;

  var xmlHttpObject = createXMLHttpRequest();
  xmlHttpObject.open("GET", encodeURI("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=" + name), true);
  xmlHttpObject.onreadystatechange = function() {
    createFacetaWithTypeSpec(xmlHttpObject, name, locationToAppend);
  };
  xmlHttpObject.send(null);
}

function createFacetaWithTypeSpec(xmlHttpObject, name, locationToAppend) {
  if (xmlHttpObject.readyState == 4 && xmlHttpObject.status == 200) {
    var tipoFaceta = JSON.parse(xmlHttpObject.responseText);
    arraySemanticaFacetas[arrayFacetas.indexOf(name)]=tipoFaceta["semântica"];
    if (tipoFaceta["discreto"] == "discreto" && tipoFaceta["semântica"] != "figura") {
      // criar hidden checkbox sections
      createFacetaCheckbox(name, locationToAppend);
    } else if(tipoFaceta.discreto == "contínuo") {
      // criar Slides
      createFacetaSlide(name, locationToAppend);
    }
  }

  //return newFaceta;
}

function createFacetaCheckbox(name, locationToAppend) {
  //var newFaceta;
  // Panel Heading Building
  var title = document.createElement("a");
  title.setAttribute("data-toggle", "collapse");
  title.setAttribute("href", "#"+name);
  title.appendChild(document.createTextNode(name));

  var h4 = document.createElement("h4");
  h4.setAttribute("class", "panel-title");
  h4.appendChild(title);

  var divPanelHeading = document.createElement("div");
  divPanelHeading.setAttribute("class", "panel-heading");
  divPanelHeading.appendChild(h4);

  var divPanelCollapse = document.createElement("div");
  divPanelCollapse.setAttribute("id", name);
  divPanelCollapse.setAttribute("class", "panel-collapse collapse");

  var divPanelDefault = document.createElement("div")
  divPanelDefault.setAttribute("class", "panel panel-default");
  divPanelDefault.appendChild(divPanelHeading);
  divPanelDefault.appendChild(divPanelCollapse);

  var divPanelGroup = document.createElement("div");
  divPanelGroup.setAttribute("class", "panel-group");
  divPanelGroup.appendChild(divPanelDefault);

  locationToAppend.appendChild(divPanelDefault);

  var xmlHttpObject = createXMLHttpRequest();
  xmlHttpObject.open("GET", encodeURI("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=" + name), true);
  xmlHttpObject.onreadystatechange = function() {
    loadFacetaDiscretaValues(xmlHttpObject, divPanelCollapse);
  };
  xmlHttpObject.send(null);
  //return newFaceta;
}

function loadFacetaDiscretaValues(xmlHttpObject, divPanelCollapse) {
  if (xmlHttpObject.readyState == 4 && xmlHttpObject.status == 200) {
    // Panel Options Building
    var ul = document.createElement("ul");
    ul.setAttribute("class", "list-group");

    var valuesFaceta = JSON.parse(xmlHttpObject.responseText);

    for (var str of valuesFaceta) {
      if (str != "") {
        var li = document.createElement("li");
        li.setAttribute("class", "list-group-item");

        var divCheckbox = document.createElement("div");
        divCheckbox.setAttribute("class", "checkbox");

        var labelCheckbox = document.createElement("label");

        var checkbox = document.createElement("input");
        checkbox.setAttribute("type", "checkbox");
        checkbox.setAttribute("value", str);
        checkbox.setAttribute("onchange", "updateFilters()");

        labelCheckbox.appendChild(checkbox);
        labelCheckbox.appendChild(document.createTextNode(str));

        divCheckbox.appendChild(labelCheckbox);
        li.appendChild(divCheckbox);
        ul.appendChild(li);
        divPanelCollapse.appendChild(ul);
      }
    }
  }
}

function createFacetaSlide(name, locationToAppend) {
  var divSlider = document.createElement("div");
  divSlider.setAttribute("class", "rangeSlider");

  var sliderTitle = document.createElement("h4");
  sliderTitle.appendChild(document.createTextNode(name));

  var low = document.createElement("b");
  //low.appendChild(document.createTextNode(min));

  var high = document.createElement("b");
  //high.appendChild(document.createTextNode(max));

  var sliderElement = document.createElement("input");
  sliderElement.setAttribute("id", name);
  sliderElement.setAttribute("type", "text");
  sliderElement.setAttribute("data-slider-step", 5);
  sliderElement.setAttribute("onchange", "updateFilters()");

  divSlider.appendChild(sliderTitle);
  divSlider.appendChild(sliderTitle);
  divSlider.appendChild(low);
  divSlider.appendChild(sliderElement);
  divSlider.appendChild(high);

  locationToAppend.appendChild(divSlider);

  facetaContinuaMin(name, low, high, sliderElement);
}

function facetaContinuaMin(name, low, high, sliderElement) {
  var xmlHttpObject = createXMLHttpRequest();
  xmlHttpObject.open("GET", encodeURI("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/minFaceta.php?facetaCont=" + name), true);
  xmlHttpObject.onreadystatechange = function() {
    loadFacetaContinuaValue(xmlHttpObject, "min", name, "data-slider-min", low, high, sliderElement);
  };
  xmlHttpObject.send(null);
}

function facetaContinuaMax(name, low, high, sliderElement) {
  var xmlHttpObject = createXMLHttpRequest();
  xmlHttpObject.open("GET", encodeURI("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/maxFaceta.php?facetaCont=" + name), true);
  xmlHttpObject.onreadystatechange = function() {
    loadFacetaContinuaValue(xmlHttpObject, "max", name, "data-slider-max", low, high, sliderElement);
  };
  xmlHttpObject.send(null);
}

function loadFacetaContinuaValue(xmlHttpObject, type, name, property, low, high, sliderElement) {
  if (xmlHttpObject.readyState == 4 && xmlHttpObject.status == 200) {
    var faceta = JSON.parse(xmlHttpObject.responseText);
    sliderElement.setAttribute(property, faceta[type]);

    if (type == "max") {
      high.appendChild(document.createTextNode(faceta[type]));
      var min = sliderElement.getAttribute("data-slider-min");
      sliderElement.setAttribute("data-slider-value", "["+min+","+faceta[type]+"]");
      var slider = new Slider("#"+name, {});
    } else {
      low.appendChild(document.createTextNode(faceta[type]));
      facetaContinuaMax(name, low, high, sliderElement);
    }
  }
}

/* Actualiza conteúdos dos Select's dos Filtros de Pesquisa dependendo do que está seleccionado */
function updateFilters() {
  var strFacetasToAPI = "";
  var facetaElements, optionsFaceta;

  for (var i = 0; i < arrayFacetas.length; i++) {
    if (document.getElementById(arrayFacetas[i]) != null) {
      facetaElements = document.getElementById(arrayFacetas[i]).getElementsByTagName("input");

      optionsFaceta = "";
      for (var j = 0; j < facetaElements.length; j++) {
        if(facetaElements[j].checked == true) {
          if (optionsFaceta.length == 0) {
            optionsFaceta += arrayFacetas[i] + "="
          }
          optionsFaceta += facetaElements[j].value + ",";
        }
      }
      if (optionsFaceta.length > 0) {
        if (strFacetasToAPI.length > 0)  {
          strFacetasToAPI += "&"+optionsFaceta;
        } else {
          strFacetasToAPI += optionsFaceta;
        }
      }
    }
  }

  var xmlHttpObject = createXMLHttpRequest();
  if (strFacetasToAPI == "") {
    document.getElementById("imoveisSearchResultDiv").style.visibility = "hidden";
  } else {
    xmlHttpObject.open("GET", encodeURI("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?"+strFacetasToAPI), true);
    xmlHttpObject.onreadystatechange = function() {
      imoveisSearchResult(xmlHttpObject);
    }
    xmlHttpObject.send(null);
  }
}

function imoveisSearchResult(xmlHttpObject) {
  if (xmlHttpObject.readyState == 4 && xmlHttpObject.status == 200) {
    var table = document.getElementById("imoveisSearchResultTable");
    var results = JSON.parse(xmlHttpObject.responseText);
    //FILTRAR CHECKBOXS
    changeFacet=null;//faceta selecionada
    for (var i = 0; i < arrayFacetas.length; i++) {
                    var facet = arrayFacetas[i];
                    if (changeFacet===null || changeFacet !== facet) {
                        var values = new Array();
                        for (var j = 0; j < results.length; j++) {
                            if (values.indexOf(results[j][facet.name])) {
                                values.push(results[j][facet.name]);
                            }
                        }
                        //values da faceta validos
                        //falata fazer o desable das não validas
                    }
                }


    var tableHeaderRowCount = 1;
    var rowCount = table.rows.length;
    for (var i = tableHeaderRowCount; i < rowCount; i++) {
      table.deleteRow(tableHeaderRowCount);
    }

    table = table.getElementsByTagName("tbody")[0];

    var row, cell;
    var showResult;
    for (var i = 0; i < results.length; i++) {
      var sliders = document.getElementsByClassName("rangeSlider");
      for (var k = 0; k < sliders.length; k++) {
        var elm = sliders[k].getElementsByTagName("input")[0];
        var faceta = elm.getAttribute("id");
        var limits = (elm.getAttribute("data-value")).split(",");

        if ((Number(results[i][faceta]) >= Number(limits[0]) &&
          Number(results[i][faceta]) <= Number(limits[1])) || results[i][faceta] == "") {
          showResult = true;
        } else {
          showResult = false;
          break;
        }
      }

      if (showResult) {
        row = table.insertRow(0);
        for (var j = 0; j < arrayFacetas.length; j++) {
          cell = row.insertCell(j);
          var divImgs = document.createElement("div");
          divImgs.style.display = "inline-block";
          if (arraySemanticaFacetas[j] == "figura") {
            if (results[i][arrayFacetas[j]].length > 0) {
              var fotosImovel = String(results[i][arrayFacetas[j]]).split(",");
              for (var id_img = 0; id_img < fotosImovel.length; id_img++) {
                var img = document.createElement("img");
                img.setAttribute("src", fotosImovel[id_img]);
                img.setAttribute("width", "80px");

                divImgs.appendChild(img);
              }
              cell.appendChild(divImgs);
            } else {
            }
          } else {
              cell.appendChild(document.createTextNode(results[i][arrayFacetas[j]]));
          }
        }
      }
    }

    document.getElementById("imoveisSearchResultDiv").style.visibility = "visible";
  }
}


/* OUTDATED: Actualiza todos os Filtros consoante os valores retornados pela API */
function updateSelectOptions(xmlHttpObject) {
  if (xmlHttpObject.readyState == 4 && xmlHttpObject.status == 200) {
    var queryResult = JSON.parse(xmlHttpObject.responseText);
    var selectElement;

    for (var i = 0; i < arrayFacetas.length; i++) {
      selectElement = document.getElementById(arrayFacetas[i]);

      /* Apagar Opções */
      while(selectElement.firstChild) {
        selectElement.removeChild(selectElement.firstChild);
      }

      var txtSelectOption;
      /* Navegar por todos os Imóveis Retornados no JSON */
      for(var j = 0; j < queryResult.length; j++) {
        txtSelectOption = queryResult[j][arrayFacetas[i]];

        if ($('#'+arrayFacetas[i]).find('option[value="'+txtSelectOption +'"]').length == 0) {
          var option = document.createElement("option");
          option.text = txtSelectOption;
          option.value = txtSelectOption;
          selectElement.add(option);
        }
      }
    }
  }
}
