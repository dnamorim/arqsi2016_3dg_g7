﻿using ImoGest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImoGest.DataAccessLayer;
using Microsoft.AspNet.Identity;

namespace ImoGest.Resources
{
    public class AuthorizeAlertOwnerAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                return false;
            }

            var rd = httpContext.Request.RequestContext.RouteData;

            int id = int.Parse(rd.Values["ID"].ToString());
            var userID = httpContext.User.Identity.GetUserId();

            Alert ad = new ImoGestContext().Alerts.Where(r => r.ID == id).FirstOrDefault();

            return ad.OwnerID == userID;
        }
    }
}