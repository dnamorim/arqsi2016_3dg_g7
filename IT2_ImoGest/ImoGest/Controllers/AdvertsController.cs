﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImoGest.DataAccessLayer;
using ImoGest.Models;
using Microsoft.AspNet.Identity;
using ImoGest.Resources;

namespace ImoGest.Controllers
{
    public class AdvertsController : Controller
    {
        private ImoGestContext db = new ImoGestContext();

        [Authorize]
        // GET: Adverts
        public ActionResult Index()
        {
            var adverts = db.Adverts.Include(a => a.RealEstate).Include(a => a.Type);
            return View(adverts.ToList());
        }

        [Authorize]
        // GET: Adverts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advert advert = db.Adverts.Find(id);
            if (advert == null)
            {
                return HttpNotFound();
            }
            return View(advert);
        }
        
        [Authorize]
        // GET: Adverts/Create
        public ActionResult Create()
        {
            ViewBag.RealEstateID = new SelectList(db.RealEstates, "ID", "Description");
            ViewBag.TypeID = new SelectList(db.AdvertTypes, "ID", "Name");
            return View();
        }

        [Authorize]
        // POST: Adverts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Price,RealEstateID,TypeID")] Advert advert)
        {
            if (ModelState.IsValid)
            {
                advert.DateCreation = DateTime.Now;
                advert.DateLastUpdate = DateTime.Now;
                advert.OwnerID = User.Identity.GetUserId();
                db.Adverts.Add(advert);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RealEstateID = new SelectList(db.RealEstates, "ID", "Description", advert.RealEstateID);
            ViewBag.TypeID = new SelectList(db.AdvertTypes, "ID", "ID", advert.TypeID);
            return View(advert);
        }

        [AuthorizeAdvertOwner]
        // GET: Adverts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advert advert = db.Adverts.Find(id);
            if (advert == null)
            {
                return HttpNotFound();
            }
            ViewBag.RealEstateID = new SelectList(db.RealEstates, "ID", "Description", advert.RealEstateID);
            ViewBag.TypeID = new SelectList(db.AdvertTypes, "ID", "ID", advert.TypeID);
            return View(advert);
        }

        [AuthorizeAdvertOwner]
        // POST: Adverts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Price,RealEstateID,TypeID,OwnerID")] Advert advert)
        {
            if (ModelState.IsValid)
            {
                advert.DateLastUpdate = DateTime.Now;
                db.Entry(advert).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RealEstateID = new SelectList(db.RealEstates, "ID", "Description", advert.RealEstateID);
            ViewBag.TypeID = new SelectList(db.AdvertTypes, "ID", "ID", advert.TypeID);
            return View(advert);
        }

        [AuthorizeAdvertOwner]
        // GET: Adverts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advert advert = db.Adverts.Find(id);
            if (advert == null)
            {
                return HttpNotFound();
            }
            return View(advert);
        }

        [AuthorizeAdvertOwner]
        // POST: Adverts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advert advert = db.Adverts.Find(id);
            db.Adverts.Remove(advert);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
