﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImoGest.DataAccessLayer;
using ImoGest.Models;

namespace ImoGest.Controllers
{
    [Authorize]
    public class RealEstateTypologiesController : Controller
    {
        private ImoGestContext db = new ImoGestContext();

        // GET: RealEstateTypologies
        public ActionResult Index()
        {
            var realEstateTypologies = db.RealEstateTypologies.Include(r => r.Parent);
            return View(realEstateTypologies.ToList());
        }

        // GET: RealEstateTypologies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateTypology realEstateTypology = db.RealEstateTypologies.Find(id);
            if (realEstateTypology == null)
            {
                return HttpNotFound();
            }
            return View(realEstateTypology);
        }

        [Authorize(Roles = "Admin")]
        // GET: RealEstateTypologies/Create
        public ActionResult Create()
        {
            ViewBag.ParentID = new SelectList(db.RealEstateTypologies, "ID", "Name");
            return View();
        }

        // POST: RealEstateTypologies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,ParentID")] RealEstateTypology realEstateTypology)
        {
            if (ModelState.IsValid)
            {
                db.RealEstateTypologies.Add(realEstateTypology);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ParentID = new SelectList(db.RealEstateTypologies, "ID", "Name", realEstateTypology.ParentID);
            return View(realEstateTypology);
        }

        [Authorize(Roles = "Admin")]
        // GET: RealEstateTypologies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateTypology realEstateTypology = db.RealEstateTypologies.Find(id);
            if (realEstateTypology == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentID = new SelectList(db.RealEstateTypologies, "ID", "Name", realEstateTypology.ParentID);
            return View(realEstateTypology);
        }

        // POST: RealEstateTypologies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,ParentID")] RealEstateTypology realEstateTypology)
        {
            if (ModelState.IsValid)
            {
                db.Entry(realEstateTypology).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ParentID = new SelectList(db.RealEstateTypologies, "ID", "Name", realEstateTypology.ParentID);
            return View(realEstateTypology);
        }

        [Authorize(Roles = "Admin")]
        // GET: RealEstateTypologies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateTypology realEstateTypology = db.RealEstateTypologies.Find(id);
            if (realEstateTypology == null)
            {
                return HttpNotFound();
            }
            return View(realEstateTypology);
        }

        // POST: RealEstateTypologies/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealEstateTypology realEstateTypology = db.RealEstateTypologies.Find(id);
            db.RealEstateTypologies.Remove(realEstateTypology);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
