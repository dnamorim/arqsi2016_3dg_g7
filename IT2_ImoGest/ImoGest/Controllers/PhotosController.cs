﻿using ImoGest.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImoGest.Controllers
{
    public class PhotosController : Controller
    {
        private ImoGestContext db = new ImoGestContext();
        
        // GET: /Photos/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = db.RealEstatePhotos.Find(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }
    }
}