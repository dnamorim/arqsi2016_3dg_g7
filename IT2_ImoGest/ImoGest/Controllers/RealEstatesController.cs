﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImoGest.DataAccessLayer;
using ImoGest.Models;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;
using ImoGest.Resources;

namespace ImoGest.Controllers
{
    public class RealEstatesController : Controller
    {
        private ImoGestContext db = new ImoGestContext();

        [Authorize]
        // GET: RealEstates
        public ActionResult Index()
        {
            var userID = User.Identity.GetUserId();
            var realEstates = db.RealEstates.Where(r => r.OwnerID == userID).Include(r => r.Localization).Include(r => r.Typology);
            return View(realEstates.ToList());
        }

        [Authorize]
        // GET: RealEstates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstate realEstate = db.RealEstates.Include(r => r.Photos).SingleOrDefault(s => s.ID == id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            return View(realEstate);
        }

        [Authorize]
        // GET: RealEstates/Create
        public ActionResult Create()
        {
            ViewBag.TypologyID = new SelectList(db.RealEstateTypologies, "ID", "FullDescription");
            return View();
        }

        [Authorize]
        // POST: RealEstates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "ID,Description,TypologyID,Area")] RealEstate realEstate, 
            [Bind(Include = "ID,Latitude,Longitude,Altitude")] Localization localization, 
            IEnumerable<HttpPostedFileBase> uploads
            ) {

            try
            {
                if (ModelState.IsValid)
                {
                    realEstate.Photos = new List<Photo>();

                    foreach (var upload in uploads)
                    {
                        if (upload != null && upload.ContentLength > 0)
                        {
                            var photo = new Photo
                            {
                                PhotoName = System.IO.Path.GetFileName(upload.FileName),
                                ContentType = upload.ContentType
                            };
                            using (var reader = new System.IO.BinaryReader(upload.InputStream))
                            {
                                photo.Content = reader.ReadBytes(upload.ContentLength);
                            }
                            realEstate.Photos.Add(photo);
                        }
                    }

                    localization.RealEstate = realEstate;
                    realEstate.Localization = localization;
                    realEstate.OwnerID = User.Identity.GetUserId();
                    db.RealEstates.Add(realEstate);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.ID = new SelectList(db.Localizations, "RealEstateID", "RealEstateID", realEstate.ID);
            ViewBag.TypologyID = new SelectList(db.RealEstateTypologies, "ID", "Name", realEstate.TypologyID);
            return View(realEstate);
        }

        [AuthorizeRealEstateOwner]
        // GET: RealEstates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstate realEstate = db.RealEstates.Find(id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            ViewBag.ID = new SelectList(db.Localizations, "RealEstateID", "RealEstateID", realEstate.ID);
            ViewBag.TypologyID = new SelectList(db.RealEstateTypologies, "ID", "Name", realEstate.TypologyID);
            return View(realEstate);
        }

        [AuthorizeRealEstateOwner]
        // POST: RealEstates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Description,TypologyID,Area,OwnerID")] RealEstate realEstate,
            [Bind(Include = "Latitude,Longitude")] Localization localization,
            IEnumerable<HttpPostedFileBase> uploads)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(realEstate).State = EntityState.Modified;
                    localization.RealEstateID = realEstate.ID;
                    db.Entry(localization).State = EntityState.Modified;
                    db.SaveChanges();

                    if (uploads?.FirstOrDefault() != null)
                    {
                        var realEstateOnDB = db.RealEstates.Find(realEstate.ID);

                        db.RealEstatePhotos.RemoveRange(
                            db.RealEstatePhotos.Where(p => p.RealEstateID == realEstate.ID));

                        realEstateOnDB.Photos = new List<Photo>();

                        foreach (var upload in uploads)
                        {
                            if (upload != null && upload.ContentLength > 0)
                            {
                                var photo = new Photo
                                {
                                    PhotoName = System.IO.Path.GetFileName(upload.FileName),
                                    ContentType = upload.ContentType
                                };
                                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                                {
                                    photo.Content = reader.ReadBytes(upload.ContentLength);
                                }
                                realEstateOnDB.Photos.Add(photo);
                            }
                        }

                        db.SaveChanges();
                    }

                    return RedirectToAction("Index");
                }
                ViewBag.ID = new SelectList(db.Localizations, "RealEstateID", "RealEstateID", realEstate.ID);
                ViewBag.TypologyID = new SelectList(db.RealEstateTypologies, "ID", "Name", realEstate.TypologyID);
                return View(realEstate);
            }
            catch(RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            return View(realEstate);
        }

        [AuthorizeRealEstateOwner]
        // GET: RealEstates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstate realEstate = db.RealEstates.Find(id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            return View(realEstate);
        }

        [AuthorizeRealEstateOwner]
        // POST: RealEstates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealEstate realEstate = db.RealEstates.Find(id);
            Localization localization = db.Localizations.Find(id);
            db.Localizations.Remove(localization);
            db.RealEstates.Remove(realEstate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
