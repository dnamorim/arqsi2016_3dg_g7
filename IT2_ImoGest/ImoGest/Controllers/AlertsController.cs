﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ImoGest.DataAccessLayer;
using ImoGest.Models;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using System.ComponentModel;
using Microsoft.AspNet.Identity;
using ImoGest.Resources;

namespace ImoGest.Controllers
{
    public class AlertsController : Controller
    {
        private ImoGestContext db = new ImoGestContext();

        [Authorize]
        // GET: Alerts
        public ActionResult Index()
        {
            var userID = User.Identity.GetUserId();
            var alerts = db.Alerts.Where(r => r.OwnerID == userID);
            return View(alerts.ToList());
        }

        [AuthorizeAlertOwner]
        // GET: Alerts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        [Authorize]
        // GET: Alerts/Create
        public ActionResult Create()
        {
            ViewBag.Field = EnumHelper.GetSelectList(typeof(Field));
            ViewBag.Constraint = EnumHelper.GetSelectList(typeof(Models.Constraint));
            return View();
        }

        [Authorize]
        // POST: Alerts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Alert alert)
        {
            ViewBag.Field = EnumHelper.GetSelectList(typeof(Field));
            ViewBag.Constraint = EnumHelper.GetSelectList(typeof(Models.Constraint));

            if (ModelState.IsValid)
            {
                if (alert.StartDateTime <= alert.EndDateTime)
                {
                    foreach (var param in alert.Parameters)
                    {
                        var result = db.AlertParameterFields.Where(pf => pf.Field == param.Field && pf.Constraint == param.Constraint);
                        if (db.AlertParameterFields.Where(pf => pf.Field == param.Field && pf.Constraint == param.Constraint).Count() <= 0) {
                            return View(alert);
                        }
                    }
                    alert.OwnerID = User.Identity.GetUserId();
                    db.Alerts.Add(alert);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View(alert);
        }

        [AuthorizeAlertOwner]
        // GET: Alerts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            ViewBag.Field = EnumHelper.GetSelectList(typeof(Field));
            ViewBag.Constraint = EnumHelper.GetSelectList(typeof(Models.Constraint));
            return View(alert);
        }

        [AuthorizeAlertOwner]
        // POST: Alerts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Message,StartDateTime,EndDateTime,OwnerID")] Alert alert)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alert).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(alert);
        }

        [AuthorizeAlertOwner]
        // GET: Alerts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        [AuthorizeAlertOwner]
        // POST: Alerts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alert alert = db.Alerts.Find(id);
            db.Alerts.Remove(alert);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
