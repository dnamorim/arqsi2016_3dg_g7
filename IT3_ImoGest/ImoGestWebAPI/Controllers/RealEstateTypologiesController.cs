﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;

namespace ImoGestWebAPI.Controllers
{
    // ADAPTED FROM: https://sachabarbs.wordpress.com/2012/07/30/asp-mvc-4-webapi-unitofwork-respository-and-ioc/
    [RoutePrefix("api/RealEstateTypologies")]
    public class RealEstateTypologiesController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        // GET: api/RealEstateTypologies
        [ResponseType(typeof(ICollection<RealEstateTypology>))]
        public IHttpActionResult GetRealEstateTypologies()
        {
            if (!unitOfWork.Authorizations.RealEstateTypology.allowView())
            {
                return Unauthorized();
            }
            return Ok(unitOfWork.Repositories.RealEstateTypologies.All());
        }

        [Route("Exists/{id}")]
        public IHttpActionResult GetExists(int id)
        {
            if (!unitOfWork.Repositories.RealEstateTypologies.Exists(id))
            {
                return NotFound();
            }
            return Ok();
        }

        [Route("IsUnique/{id}")]
        public IHttpActionResult GetUniqueTypology(int id)
        {
            RealEstateTypology realEstateTypology = unitOfWork.Repositories.RealEstateTypologies.GetById(id);
            if (realEstateTypology == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Repositories.RealEstateTypologies.IsUnique(realEstateTypology))
            {
                return NotFound();
            }
            return Ok();
        }


        // GET: api/RealEstateTypologies/5
        [ResponseType(typeof(RealEstateTypology))]
        public IHttpActionResult GetRealEstateTypology(int id)
        {
            RealEstateTypology realEstateTypology = unitOfWork.Repositories.RealEstateTypologies.GetById(id);
            if (realEstateTypology == null)
            {
                return NotFound();
            }
            if (!unitOfWork.Authorizations.RealEstateTypology.allowDetails(realEstateTypology))
            {
                return Unauthorized();
            }

            return Ok(realEstateTypology);
        }

        // PUT: api/RealEstateTypologies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRealEstateTypology(int id, RealEstateTypology realEstateTypology)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != realEstateTypology.ID)
            {
                return BadRequest();
            }

            if (!unitOfWork.Authorizations.RealEstateTypology.allowEdit(realEstateTypology))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.RealEstateTypologies.Edit(realEstateTypology);

            try
            {
                unitOfWork.Save();
            } catch(DbUpdateConcurrencyException)
            {
                if (unitOfWork.Repositories.RealEstateTypologies.Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RealEstateTypologies
        [ResponseType(typeof(RealEstateTypology))]
        public IHttpActionResult PostRealEstateTypology(RealEstateTypology realEstateTypology)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!unitOfWork.Authorizations.RealEstateTypology.allowAdd())
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.RealEstateTypologies.Add(realEstateTypology);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = realEstateTypology.ID }, realEstateTypology);
        }

        // DELETE: api/RealEstateTypologies/5
        [ResponseType(typeof(RealEstateTypology))]
        public IHttpActionResult DeleteRealEstateTypology(int id)
        {
            RealEstateTypology realEstateTypology = unitOfWork.Repositories.RealEstateTypologies.GetById(id);
            if (realEstateTypology == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstateTypology.allowDelete(realEstateTypology))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.RealEstateTypologies.Delete(realEstateTypology);
            unitOfWork.Save();

            return Ok(realEstateTypology);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RealEstateTypologyExists(int id)
        {
            return unitOfWork.Repositories.RealEstateTypologies.Exists(id);
        }
    }
}