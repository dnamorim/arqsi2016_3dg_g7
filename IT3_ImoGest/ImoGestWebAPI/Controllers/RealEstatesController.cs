﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/RealEstates")]
    public class RealEstatesController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        // GET: api/RealEstates
        [ResponseType(typeof(ICollection<RealEstate>))]
        public IHttpActionResult GetRealEstates()
        {
            if (!unitOfWork.Authorizations.RealEstate.allowView())
            {
                return Unauthorized();
            }
            return Ok(unitOfWork.Repositories.RealEstates.All());
        }

        // GET: api/RealEstates/User
        [Route("User")]
        [ResponseType(typeof(ICollection<RealEstate>))]
        public IHttpActionResult GetAllFromUser()
        {
            if (!unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client))
            {
                return Unauthorized();
            }

            return Ok(unitOfWork.Repositories.RealEstates.AllRealEstatesFromUser());
        }

        // GET: api/RealEstates/5
        [Route("{id}")]
        [ResponseType(typeof(RealEstate))]
        public IHttpActionResult GetRealEstate(int id)
        {
            RealEstate realEstate = unitOfWork.Repositories.RealEstates.GetById(id);
            if (realEstate == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstate.allowDetails(realEstate))
            {
                return Unauthorized();
            }

            return Ok(realEstate);
        }

        [Route("Photo/{id}")]
        [ResponseType(typeof(Photo))]
        public IHttpActionResult GetPhoto(int id)
        {
            Photo photo = unitOfWork.Repositories.RealEstates.GetPhotoById(id);
            if (photo == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstate.allowView())
            {
                return Unauthorized();
            }
            return Ok(photo);
        }

        // GET: api/RealEstates/Exists/5
        [Route("Exists/{id}")]
        public IHttpActionResult GetExists(int id)
        {
            if (!unitOfWork.Repositories.Adverts.Exists(id))
            {
                return NotFound();
            }
            return Ok();
        }

        // PUT: api/RealEstates/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRealEstate(int id, RealEstate realEstate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != realEstate.ID)
            {
                return BadRequest();
            }

            if (!unitOfWork.Authorizations.RealEstate.allowEdit(realEstate))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.RealEstates.Edit(realEstate);

            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RealEstateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RealEstates
        [ResponseType(typeof(RealEstate))]
        public IHttpActionResult PostRealEstate(RealEstate realEstate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!unitOfWork.Authorizations.RealEstate.allowAdd())
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.RealEstates.Add(realEstate);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = realEstate.ID }, realEstate);
        }

        // DELETE: api/RealEstates/5
        [ResponseType(typeof(RealEstate))]
        public IHttpActionResult DeleteRealEstate(int id)
        {
            RealEstate realEstate = unitOfWork.Repositories.RealEstates.GetById(id);
            if (realEstate == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstate.allowDelete(realEstate))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.RealEstates.Delete(realEstate);
            unitOfWork.Save();

            return Ok(realEstate);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RealEstateExists(int id)
        {
            return unitOfWork.Repositories.RealEstates.Exists(id);
        }
    }
}