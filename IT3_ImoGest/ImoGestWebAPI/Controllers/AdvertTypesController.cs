﻿using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/AdvertTypes")]
    public class AdvertTypesController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        // GET: api/AdvertTypes
        [ResponseType(typeof(ICollection<AdvertType>))]
        public IHttpActionResult GetAdvertTypes()
        {
            if (!unitOfWork.Authorizations.ApplicationUser.IsAuthenticated)
            {
                return Unauthorized();
            }
            return Ok(unitOfWork.Repositories.AdvertTypes.All());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
