﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/Alerts")]
    public class AlertsController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        // GET: api/Alerts/Fields
        [Route("Fields")]
        [ResponseType(typeof(ICollection<System.Web.Mvc.SelectListItem>))]
        public IHttpActionResult GetAllFields()
        {
            return Ok(unitOfWork.Repositories.Alerts.GetAlertFields());
        }

        [Route("Constraints")]
        [ResponseType(typeof(ICollection<System.Web.Mvc.SelectListItem>))]
        // GET: api/Alerts/Constraints
        public IHttpActionResult GetAllConstraints()
        {
            return Ok(unitOfWork.Repositories.Alerts.GetAlertConstraints());
        }

        // GET: api/Alerts/User
        [Route("User")]
        [ResponseType(typeof(ICollection<Alert>))]
        public IHttpActionResult GetAllFromUser()
        {
            if (!unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client))
            {
                return Unauthorized();
            }

            return Ok(unitOfWork.Repositories.Alerts.AllFromUser());
        }

        // GET: api/Alerts/User
        [Route("Exists/{id}")]
        public IHttpActionResult GetExists(int id)
        {
            if (!unitOfWork.Repositories.Alerts.Exists(id))
            {
                return NotFound();
            }

            return Ok();
        }

        // GET: api/Alerts/5
        [ResponseType(typeof(Alert))]
        public IHttpActionResult GetAlert(int id)
        {
            Alert alert = unitOfWork.Repositories.Alerts.GetById(id);

            if (alert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Alert.allowDetails(alert))
            {
                return Unauthorized();
            }

            return Ok(alert);
        }

        // PUT: api/Alerts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAlert(int id, Alert alert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != alert.ID)
            {
                return BadRequest();
            }

            if (!unitOfWork.Authorizations.Alert.allowEdit(alert))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.Alerts.Edit(alert);

            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlertExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Alerts
        [ResponseType(typeof(Alert))]
        public IHttpActionResult PostAlert(Alert alert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!unitOfWork.Authorizations.Alert.allowAdd())
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.Alerts.Add(alert);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = alert.ID }, alert);
        }

        // DELETE: api/Alerts/5
        [ResponseType(typeof(Alert))]
        public IHttpActionResult DeleteAlert(int id)
        {
            Alert alert = unitOfWork.Repositories.Alerts.GetById(id);
            if (alert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Alert.allowDelete(alert))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.Alerts.Remove(alert);
            unitOfWork.Save();

            return Ok(alert);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AlertExists(int id)
        {
            return unitOfWork.Repositories.Alerts.Exists(id);
        }
    }
}