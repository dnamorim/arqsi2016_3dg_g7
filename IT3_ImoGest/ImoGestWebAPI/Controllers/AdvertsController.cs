﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/Adverts")]
    public class AdvertsController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        // GET: api/Adverts
        [ResponseType(typeof(ICollection<Advert>))]
        public IHttpActionResult GetAdverts()
        {
            if (!unitOfWork.Authorizations.Advert.allowView())
            {
                return Unauthorized();
            }
            return Ok(unitOfWork.Repositories.Adverts.All());
        }

        // GET: api/Adverts/Exists/5
        [Route("Exists/{id}")]
        public IHttpActionResult GetExists(int id)
        {
            if (!unitOfWork.Repositories.Adverts.Exists(id))
            {
                return NotFound();
            }

            return Ok();
        }

        // GET: api/Adverts/5
        [ResponseType(typeof(Advert))]
        public IHttpActionResult GetAdvert(int id)
        {
            Advert advert = unitOfWork.Repositories.Adverts.GetById(id);
            if (advert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Advert.allowDetails(advert))
            {
                return Unauthorized();
            }

            return Ok(advert);
        }

        // PUT: api/Adverts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAdvert(int id, Advert advert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != advert.ID)
            {
                return BadRequest();
            }

            if (!unitOfWork.Authorizations.Advert.allowEdit(advert))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.Adverts.Edit(advert);

            try
            {
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdvertExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Adverts
        [ResponseType(typeof(Advert))]
        public IHttpActionResult PostAdvert(Advert advert)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!unitOfWork.Authorizations.Advert.allowAdd())
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.Adverts.Add(advert);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = advert.ID }, advert);
        }

        // DELETE: api/Adverts/5
        [ResponseType(typeof(Advert))]
        public IHttpActionResult DeleteAdvert(int id)
        {
            Advert advert = unitOfWork.Repositories.Adverts.GetById(id);
            if (advert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Advert.allowDelete(advert))
            {
                return Unauthorized();
            }

            unitOfWork.Repositories.Adverts.Remove(advert);
            unitOfWork.Save();

            return Ok(advert);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdvertExists(int id)
        {
            return unitOfWork.Repositories.Adverts.Exists(id);
        }
    }
}