﻿using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/Authorization/Alert")]
    public class AlertAutorizationController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        [Route("allowAdd")]
        public IHttpActionResult GetallowAdd()
        {
            if (!unitOfWork.Authorizations.Alert.allowAdd())
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowEdit/{id}")]
        public IHttpActionResult GetallowEdit(int id)
        {
            Alert alert = unitOfWork.Repositories.Alerts.GetById(id);

            if (alert == null)
            {
                return BadRequest();

            }

            if (!unitOfWork.Authorizations.Alert.allowEdit(alert))
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowView")]
        public IHttpActionResult GetallowView()
        {
            if (!unitOfWork.Authorizations.Alert.allowView())
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowDelete/{id}")]
        public IHttpActionResult GetallowDelete(int id)
        {
            Alert alert = unitOfWork.Repositories.Alerts.GetById(id);

            if (alert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Alert.allowDelete(alert))
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowDetails/{id}")]
        public IHttpActionResult GetallowDetails(int id)
        {
            Alert alert = unitOfWork.Repositories.Alerts.GetById(id);

            if (alert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Alert.allowDetails(alert))
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("isOwner/{id}")]
        public IHttpActionResult GetisOwner(int id)
        {
            Alert alert = unitOfWork.Repositories.Alerts.GetById(id);

            if (alert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Alert.isOwner(alert))
            {
                return Unauthorized();
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (unitOfWork != null)
                {
                    unitOfWork.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}