﻿using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/Authorization/RealEstate")]
    public class RealEstateAuthorizationController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        [Route("allowAdd")]
        public IHttpActionResult GetallowrealEstated()
        {
            if (!unitOfWork.Authorizations.RealEstate.allowAdd())
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowEdit/{id}")]
        public IHttpActionResult GetallowEdit(int id)
        {
            RealEstate RealEstate = unitOfWork.Repositories.RealEstates.GetById(id);

            if (RealEstate == null)
            {
                return NotFound();

            }

            if (!unitOfWork.Authorizations.RealEstate.allowEdit(RealEstate))
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowView")]
        public IHttpActionResult GetallowView()
        {
            if (!unitOfWork.Authorizations.RealEstate.allowView())
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowDelete/{id}")]
        public IHttpActionResult GetcanDelete(int id)
        {
            RealEstate realEstate = unitOfWork.Repositories.RealEstates.GetById(id);

            if (realEstate == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstate.allowDelete(realEstate))
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowDetails/{id}")]
        public IHttpActionResult GetcanDetails(int id)
        {
            RealEstate realEstate = unitOfWork.Repositories.RealEstates.GetById(id);

            if (realEstate == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstate.allowDetails(realEstate))
            {
                return Unauthorized();
            }

            return Ok();
        }



        [Route("isOwner/{id}")]
        public IHttpActionResult GetisOwner(int id)
        {
            RealEstate realEstate = unitOfWork.Repositories.RealEstates.GetById(id);

            if (realEstate == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstate.isOwner(realEstate))
            {
                return Unauthorized();
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (unitOfWork != null)
                {
                    unitOfWork.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
