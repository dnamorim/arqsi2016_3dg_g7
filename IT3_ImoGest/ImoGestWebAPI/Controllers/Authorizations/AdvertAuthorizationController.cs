﻿using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/Authorization/Advert")]
    public class AdvertAuthorizationController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        [Route("allowAdd")]
        public IHttpActionResult GetallowAdd()
        {
            if (!unitOfWork.Authorizations.Advert.allowAdd())
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowEdit/{id}")]
        public IHttpActionResult GetallowEdit(int id)
        {
            Advert advert = unitOfWork.Repositories.Adverts.GetById(id);

            if (advert == null)
            {
                return NotFound();

            }

            if (!unitOfWork.Authorizations.Advert.allowEdit(advert))
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowView")]
        public IHttpActionResult GetallowView()
        {
            if (!unitOfWork.Authorizations.Advert.allowView())
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowDelete/{id}")]
        public IHttpActionResult GetcanDelete(int id)
        {
            Advert advert = unitOfWork.Repositories.Adverts.GetById(id);

            if (advert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Advert.allowDelete(advert))
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowDetails/{id}")]
        public IHttpActionResult GetcanDetails(int id)
        {
            Advert advert = unitOfWork.Repositories.Adverts.GetById(id);

            if (advert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Advert.allowDetails(advert))
            {
                return Unauthorized();
            }

            return Ok();
        }



        [Route("isOwner/{id}")]
        public IHttpActionResult GetisOwner(int id)
        {
            Advert advert = unitOfWork.Repositories.Adverts.GetById(id);

            if (advert == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.Advert.isOwner(advert))
            {
                return Unauthorized();
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (unitOfWork != null)
                {
                    unitOfWork.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
