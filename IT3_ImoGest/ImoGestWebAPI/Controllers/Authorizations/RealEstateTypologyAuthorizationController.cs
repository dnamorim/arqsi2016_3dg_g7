﻿using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/Authorization/RealEstateTypology")]
    public class RealEstateTypologyAutorizationController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        [Route("allowAdd")]
        public IHttpActionResult GetallowAdd()
        {
            if (!unitOfWork.Authorizations.RealEstateTypology.allowAdd())
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowEdit/{id}")]
        public IHttpActionResult GetallowEdit(int id)
        {
            RealEstateTypology realEstateTypology = unitOfWork.Repositories.RealEstateTypologies.GetById(id);

            if (realEstateTypology == null)
            {
                return BadRequest();

            }

            if (!unitOfWork.Authorizations.RealEstateTypology.allowEdit(realEstateTypology))
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowView")]
        public IHttpActionResult GetallowView()
        {
            if (!unitOfWork.Authorizations.RealEstateTypology.allowView())
            {
                return Unauthorized();
            }

            return Ok();
        }

        [Route("allowDelete/{id}")]
        public IHttpActionResult GetallowDelete(int id)
        {
            RealEstateTypology realEstateTypology = unitOfWork.Repositories.RealEstateTypologies.GetById(id);

            if (realEstateTypology == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstateTypology.allowDelete(realEstateTypology))
            {
                return Unauthorized();
            }

            return Ok();
        }


        [Route("allowDetails/{id}")]
        public IHttpActionResult GetallowDetails(int id)
        {
            RealEstateTypology realEstateTypology = unitOfWork.Repositories.RealEstateTypologies.GetById(id);

            if (realEstateTypology == null)
            {
                return NotFound();
            }

            if (!unitOfWork.Authorizations.RealEstateTypology.allowDetails(realEstateTypology))
            {
                return Unauthorized();
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (unitOfWork != null)
                {
                    unitOfWork.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}
