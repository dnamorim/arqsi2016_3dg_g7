﻿using ImoGestClassLibrary.DataAccessLayer;
using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ImoGestWebAPI.Controllers
{
    [RoutePrefix("api/ApplicationUsers")]
    public class ApplicationUsersController : ApiController
    {
        private UnitOfWork unitOfWork = UnitOfWork.Instance();

        // GET: api/Users/Mediators
        [Route("Mediators")]
        [ResponseType(typeof(ICollection<RealEstate>))]
        public IHttpActionResult GetAllMediators()
        {
            if (!unitOfWork.Authorizations.ApplicationUser.IsAuthenticated)
            {
                return Unauthorized();
            }
            return Ok(unitOfWork.Repositories.ApplicationUsers.GetAllMediators());
        }
    }
}
