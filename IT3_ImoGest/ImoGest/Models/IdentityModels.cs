﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Mvc;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;

namespace ImoGestClient.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class UserApi
    {
        public string Id { get; set; }
        public ICollection<string> Roles { get; set; }
        public TokenApi Token { get; set; }
    }

    public class TokenApi
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        [JsonProperty("userName")]
        public string Username { get; set; }
        [JsonProperty(".issued")]
        public DateTime IssuedAt { get; set; }
        [JsonProperty(".expires")]
        public DateTime ExpiresAt { get; set; }
    }

    public static class UserRolesApi
    {
        public const string Administrator = "Administrator";
        public const string Client = "Client";
        public const string Mediator = "Mediator";
    }
}