﻿using ImoGestClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace ImoGestClient
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        // ADAPTED FROM: http://stackoverflow.com/questions/1064271/asp-net-mvc-set-custom-iidentity-or-iprincipal
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {

            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName + "_bearer"];

            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                UserApi user = Newtonsoft.Json.JsonConvert.DeserializeObject<UserApi>(authTicket.UserData);

                GenericIdentity MyIdentity = new GenericIdentity(authTicket.Name, "bearer");

                List<Claim> MyClaims = new List<Claim>{
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", authTicket.Name),
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", user.Id),
                    new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/token", user.Token.AccessToken)
                };
                MyIdentity.AddClaims(MyClaims);

                GenericPrincipal MyPrincipal =
                    new GenericPrincipal(MyIdentity, user.Roles.ToArray());

                HttpContext.Current.User = MyPrincipal;

            }
        }
    }
}
