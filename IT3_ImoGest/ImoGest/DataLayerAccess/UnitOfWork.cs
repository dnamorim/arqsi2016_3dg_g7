﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;

namespace ImoGestClient.DataLayerAccess
{
    public class UnitOfWork
    {
        public string ClientConnection { get; }

        public JsonSerializerSettings SerializerSettings { get; }

        private UnitOfWork(IPrincipal user, string clientConnection, JsonSerializerSettings serializerSettings)
        {
            this._currentUser = user;
            this.ClientConnection = clientConnection;
            this.SerializerSettings = serializerSettings;
        }

        public HttpClient Client
        { 
            get
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(ClientConnection);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                if (Authorizations.ApplicationUser.IsAuthenticated && CurrentUser.Identity is GenericIdentity)
                {
                    string token = ((GenericIdentity)CurrentUser.Identity).Claims.Single(m => m.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/token").Value;
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(Authorizations.ApplicationUser.AuthenticationType, token);
                }
                return client;
            }
        }

        private IPrincipal _currentUser;
        public IPrincipal CurrentUser
        {
            get
            {
                return _currentUser ?? (_currentUser = HttpContext.Current.User);
            }
        }

        private Authorizations _authorizations;
        public Authorizations Authorizations
        {
            get
            {
                return _authorizations ?? (_authorizations = Authorizations.Instance(this));
            }
        }

        private Repositories _repositories;
        public Repositories Repositories
        {
            get
            {
                return _repositories ?? (_repositories = Repositories.Instance(this));
            }
        }

        public void Save()
        {

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public static UnitOfWork Instance(IPrincipal user = null, string clientConnection = null, JsonSerializerSettings serializerSettings = null)
        {
            if (clientConnection == null)
            {
                clientConnection = "";
                var connection = WebConfigurationManager.AppSettings["ClientConnection"];
                if (connection != null)
                {
                    clientConnection = connection;
                }
            }
            if (serializerSettings == null)
            {
                serializerSettings = new JsonSerializerSettings()
                {
                    Formatting = Formatting.Indented,
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    NullValueHandling = NullValueHandling.Include
                };
            }
            return new UnitOfWork(user, clientConnection, serializerSettings);
        }

    }
}