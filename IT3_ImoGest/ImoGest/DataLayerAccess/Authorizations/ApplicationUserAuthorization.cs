﻿using Microsoft.AspNet.Identity;

namespace ImoGestClient.DataLayerAccess
{
    public class ApplicationUserAuthorization
    {
        private UnitOfWork unitOfWork;

        public ApplicationUserAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool IsAuthenticated
        {
            get
            {
                return ((unitOfWork.CurrentUser?.Identity != null) && (unitOfWork.CurrentUser.Identity.IsAuthenticated));
            }
        }
        public string UserId
        {
            get {
                return (IsAuthenticated) ? unitOfWork.CurrentUser.Identity.GetUserId() : "";
            }
        }

        public string UserName
        {
            get
            {
                return (IsAuthenticated) ? unitOfWork.CurrentUser.Identity.GetUserName() : "";
            }
        }

        public string AuthenticationType
        {
            get
            {
                return (IsAuthenticated) ? unitOfWork.CurrentUser.Identity.AuthenticationType : "";
            }
        }

        public bool IsInRole(string role)
        {
            return (IsAuthenticated && unitOfWork.CurrentUser.IsInRole(role));
        }
    }
}