﻿using ImoGestClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ImoGestClient.DataLayerAccess
{
    public class AdvertAuthorization
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/Authorization/Advert";

        public AdvertAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowAdd", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowView()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowView", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDelete(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDelete/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowEdit(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowEdit/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDetails(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDetails/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool isOwner(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/isOwner/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }
    }
}