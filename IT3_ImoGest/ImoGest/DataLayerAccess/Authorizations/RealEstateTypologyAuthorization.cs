﻿using ImoGestClient.ViewModels;
using System.Net.Http;

namespace ImoGestClient.DataLayerAccess
{
    public class RealEstateTypologyAuthorization
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/Authorization/RealEstateTypology";

        public RealEstateTypologyAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowAdd", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowView()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowView", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDelete(RealEstateTypologyViewModels realEstateTypology)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDelete/{1}", apiAddress, realEstateTypology.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowEdit(RealEstateTypologyViewModels realEstateTypology)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowEdit/{1}", apiAddress, realEstateTypology.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDetails(RealEstateTypologyViewModels realEstateTypology)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDetails/{1}", apiAddress, realEstateTypology.ID)).Result;
            return response.IsSuccessStatusCode;
        }
    }
}