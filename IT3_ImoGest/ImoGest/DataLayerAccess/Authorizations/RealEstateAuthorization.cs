﻿using ImoGestClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace ImoGestClient.DataLayerAccess
{
    public class RealEstateAuthorization
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/Authorization/RealEstate";

        public RealEstateAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowAdd", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowView()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowView", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDelete(RealEstateViewModels realEstate)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDelete/{1}", apiAddress, realEstate.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowEdit(RealEstateViewModels realEstate)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowEdit/{1}", apiAddress, realEstate.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDetails(RealEstateViewModels realEstate)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDetails/{1}", apiAddress, realEstate.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool isOwner(AlertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/isOwner/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }
    }
}