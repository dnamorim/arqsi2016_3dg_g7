﻿using ImoGestClient.ViewModels;
using System.Net.Http;

namespace ImoGestClient.DataLayerAccess
{
    public class AlertAuthorization
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/Authorization/Alert";

        public AlertAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowAdd", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowView()
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowView", apiAddress)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDelete(AlertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDelete/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowEdit(AlertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowEdit/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool allowDetails(AlertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/allowDetails/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }

        public bool isOwner(AlertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            HttpResponseMessage response = client.GetAsync(string.Format("{0}/isOwner/{1}", apiAddress, advert.ID)).Result;
            return response.IsSuccessStatusCode;
        }
    }
}