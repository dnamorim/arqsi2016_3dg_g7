﻿using ImoGestClient.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ImoGestClient.DataLayerAccess
{
    public class RealEstateTypologyRepository
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/RealEstateTypologies";

        public RealEstateTypologyRepository(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public RealEstateTypologyViewModels Add(RealEstateTypologyViewModels realEstateTypology)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(realEstateTypology, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PostAsync(apiAddress, content).Result;

            return (response.IsSuccessStatusCode) ? realEstateTypology : null;
        }

        public RealEstateTypologyViewModels Edit(RealEstateTypologyViewModels realEstateTypology)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(realEstateTypology, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PutAsync(string.Format("{0}/{1}", apiAddress, realEstateTypology.ID), content).Result;

            return (response.IsSuccessStatusCode) ? realEstateTypology : null;
        }

        public RealEstateTypologyViewModels Remove(RealEstateTypologyViewModels realEstateTypology)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.DeleteAsync(string.Format("{0}/{1}", apiAddress, realEstateTypology.ID)).Result;

            return (response.IsSuccessStatusCode) ? realEstateTypology : null;
        }

        public bool Exists(int id)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.GetAsync(string.Format("{0}/Exists/{1}", apiAddress, id)).Result;

            return response.IsSuccessStatusCode;
        }

        public ICollection<RealEstateTypologyViewModels> All()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(apiAddress).Result;

            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<RealEstateTypologyViewModels>>(content, unitOfWork.SerializerSettings);
            }

            return new List<RealEstateTypologyViewModels>();
        }

        public virtual RealEstateTypologyViewModels GetById(int id)
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/{1}", apiAddress, id)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<RealEstateTypologyViewModels>(content, unitOfWork.SerializerSettings);
            }

            return null;
        }
    }
}