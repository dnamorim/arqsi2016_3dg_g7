﻿using ImoGestClient.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ImoGestClient.DataLayerAccess
{
    public class RealEstateRepository
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/RealEstates";

        public RealEstateRepository(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public RealEstateViewModels Add(RealEstateViewModels realEstate)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(realEstate, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PostAsync(apiAddress, content).Result;

            return (response.IsSuccessStatusCode) ? realEstate : null;
        }

        public RealEstateViewModels Edit(RealEstateViewModels realEstate)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(realEstate, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PutAsync(string.Format("{0}/{1}", apiAddress, realEstate.ID), content).Result;

            return (response.IsSuccessStatusCode) ? realEstate : null;
        }

        public RealEstateViewModels Remove(RealEstateViewModels realEstate)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.DeleteAsync(string.Format("{0}/{1}", apiAddress, realEstate.ID)).Result;

            return (response.IsSuccessStatusCode) ? realEstate : null;
        }

        public PhotoViewModels GetPhotoById(int id)
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/Photo/{1}", apiAddress, id)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<PhotoViewModels>(content, unitOfWork.SerializerSettings);
            }

            return null;
        }

        public bool Exists(int id)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.GetAsync(string.Format("{0}/Exists/{1}", apiAddress, id)).Result;

            return response.IsSuccessStatusCode;
        }

        public ICollection<RealEstateViewModels> All()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(apiAddress).Result;

            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<RealEstateViewModels>>(content, unitOfWork.SerializerSettings);
            }

            return new List<RealEstateViewModels>();
        }

        public ICollection<RealEstateViewModels> AllFromUser()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/User", apiAddress)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<RealEstateViewModels>>(content, unitOfWork.SerializerSettings);
            }

            return new List<RealEstateViewModels>();
        }

        public virtual RealEstateViewModels GetById(int id)
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/{1}", apiAddress, id)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<RealEstateViewModels>(content, unitOfWork.SerializerSettings);
            }

            return null;
        }

    }
}