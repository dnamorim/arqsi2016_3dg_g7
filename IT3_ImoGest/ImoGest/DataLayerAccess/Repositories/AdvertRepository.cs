﻿using ImoGestClient.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ImoGestClient.DataLayerAccess
{
    public class AdvertRepository
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/Adverts";

        public AdvertRepository(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public AdvertViewModels Add(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(advert, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PostAsync(apiAddress, content).Result;

            return (response.IsSuccessStatusCode) ? advert : null;
        }

        public AdvertViewModels Edit(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(advert, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PutAsync(string.Format("{0}/{1}", apiAddress, advert.ID), content).Result;

            return (response.IsSuccessStatusCode) ? advert : null;
        }

        public AdvertViewModels Remove(AdvertViewModels advert)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.DeleteAsync(string.Format("{0}/{1}", apiAddress, advert.ID)).Result;

            return (response.IsSuccessStatusCode) ? advert : null;
        }

        public bool Exists(int id)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.GetAsync(string.Format("{0}/Exists/{1}", apiAddress, id)).Result;

            return response.IsSuccessStatusCode;
        }

        public ICollection<AdvertViewModels> All()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(apiAddress).Result;

            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<AdvertViewModels>>(content, unitOfWork.SerializerSettings);
            }

            return new List<AdvertViewModels>();
        }

        public virtual AdvertViewModels GetById(int id)
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/{1}", apiAddress, id)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<AdvertViewModels>(content, unitOfWork.SerializerSettings);
            }

            return null;
        }
    }
}