﻿using ImoGestClient.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;

namespace ImoGestClient.DataLayerAccess
{
    public class AlertRepository
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/Alerts";

        public AlertRepository(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public AlertViewModels Add(AlertViewModels alert)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(alert, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PostAsync(apiAddress, content).Result;

            return (response.IsSuccessStatusCode) ? alert : null;
        }

        public AlertViewModels Edit(AlertViewModels alert)
        {
            HttpClient client = unitOfWork.Client;
            string jsonSerializedString = JsonConvert.SerializeObject(alert, unitOfWork.SerializerSettings);
            HttpContent content = new StringContent(jsonSerializedString, System.Text.Encoding.Unicode, "application/json");
            var response = client.PutAsync(string.Format("{0}/{1}", apiAddress, alert.ID), content).Result;

            return (response.IsSuccessStatusCode) ? alert : null;
        }

        public AlertViewModels Remove(AlertViewModels alert)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.DeleteAsync(string.Format("{0}/{1}", apiAddress, alert.ID)).Result;

            return (response.IsSuccessStatusCode) ? alert : null;
        }

        public bool Exists(int id)
        {
            HttpClient client = unitOfWork.Client;
            var response = client.GetAsync(string.Format("{0}/Exists/{1}", apiAddress, id)).Result;

            return response.IsSuccessStatusCode;
        }

        public ICollection<AlertViewModels> AllFromUser()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/User",apiAddress)).Result;

            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<AlertViewModels>>(content, unitOfWork.SerializerSettings);
            }

            return new List<AlertViewModels>();
        }

        public virtual AlertViewModels GetById(int id)
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/{1}", apiAddress, id)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<AlertViewModels>(content, unitOfWork.SerializerSettings);
            }

            return null;
        }

        public ICollection<SelectListItem> GetAllFields()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/Fields", apiAddress)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<SelectListItem>>(content, unitOfWork.SerializerSettings);
            }
            return new List<SelectListItem>();
        }

        public ICollection<SelectListItem> GetAllConstraints()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(string.Format("{0}/Constraints", apiAddress)).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<SelectListItem>>(content, unitOfWork.SerializerSettings);
            }
            return new List<SelectListItem>();
        }
    }
}