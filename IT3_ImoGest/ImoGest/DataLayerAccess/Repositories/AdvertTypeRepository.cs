﻿using ImoGestClient.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;

namespace ImoGestClient.DataLayerAccess
{
    public class AdvertTypeRepository
    {
        private UnitOfWork unitOfWork;
        private const string apiAddress = "api/AdvertTypes";

        public AdvertTypeRepository(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public ICollection<AdvertTypeViewModels> All()
        {
            HttpResponseMessage response = unitOfWork.Client.GetAsync(apiAddress).Result;
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<ICollection<AdvertTypeViewModels>>(content, unitOfWork.SerializerSettings);
            }
            return new List<AdvertTypeViewModels>();
        }
    }
}