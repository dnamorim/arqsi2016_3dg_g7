﻿using ImoGestClient.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImoGestClient.Controllers
{
    public class HomeController : MainController
    {
        public ActionResult Index()
        {
            if (UnitOfWork.Authorizations.ApplicationUser.IsInRole(UserRolesApi.Administrator))
            {
                return RedirectToAction("Index", "RealEstateTypologies");
            }

            if (UnitOfWork.Authorizations.ApplicationUser.IsInRole(UserRolesApi.Client))
            {
                return RedirectToAction("Index", "Adverts");
            }
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}