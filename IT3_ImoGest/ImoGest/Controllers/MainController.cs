﻿using ImoGestClient.DataLayerAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ImoGestClient.Controllers
{
   public static class AuthenticationBaseExtensions
    {
        public static Authorizations Authorizations(this HtmlHelper helper)
        {
            ControllerBase controller = helper.ViewContext.Controller;
            if (controller is MainController)
            {
                return ((MainController)controller).UnitOfWork.Authorizations;
            }
            throw new HttpException(501, "HTTP/1.1 501 Not Implemented");
        }
    }

    public abstract class MainController : Controller
    {

        public UnitOfWork UnitOfWork = UnitOfWork.Instance();

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                UnitOfWork?.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}