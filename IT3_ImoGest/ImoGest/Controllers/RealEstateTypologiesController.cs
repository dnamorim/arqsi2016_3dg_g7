﻿using ImoGestClient.Models;
using ImoGestClient.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ImoGestClient.Controllers
{
    public class RealEstateTypologiesController : MainController
    {
        [Authorize(Roles = UserRolesApi.Administrator+","+UserRolesApi.Client)]
        // GET: RealEstateTypologies
        public ActionResult Index()
        {
            if (!UnitOfWork.Authorizations.RealEstateTypology.allowView())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(UnitOfWork.Repositories.RealEstateTypologies.All());
        }

        // GET: RealEstateTypologies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateTypologyViewModels realEstateTypology = UnitOfWork.Repositories.RealEstateTypologies.GetById((int) id);
            if (realEstateTypology == null)
            {
                return HttpNotFound();
            }
            if (!UnitOfWork.Authorizations.RealEstateTypology.allowDetails(realEstateTypology))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(realEstateTypology);
        }

        // GET: RealEstateTypologies/Create
        [Authorize(Roles = UserRolesApi.Administrator)]
        public ActionResult Create()
        {
            if (!UnitOfWork.Authorizations.RealEstateTypology.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.ParentID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All(), "ID", "FullDescription").ToList();
            return View();
        }

        // POST: RealEstateTypologies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = UserRolesApi.Administrator)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,ParentID")] RealEstateTypologyViewModels realEstateTypology)
        {
            if (!UnitOfWork.Authorizations.RealEstateTypology.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid)
            {
                UnitOfWork.Repositories.RealEstateTypologies.Add(realEstateTypology);
                UnitOfWork.Save();
                return RedirectToAction("Index");
            }

            ViewBag.ParentID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All(), "ID", "FullDescription").ToList();
            return View(realEstateTypology);
        }

        // GET: RealEstateTypologies/Edit/5
        [Authorize(Roles = UserRolesApi.Administrator)]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateTypologyViewModels realEstateTypology = UnitOfWork.Repositories.RealEstateTypologies.GetById((int) id);
            if (realEstateTypology == null)
            {
                return HttpNotFound();
            }
            if (!UnitOfWork.Authorizations.RealEstateTypology.allowEdit(realEstateTypology))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            ViewBag.ParentID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All().Where(a => a.ID != realEstateTypology.ID) , "ID", "Name", realEstateTypology.ParentID);
            return View(realEstateTypology);
        }

        // POST: RealEstateTypologies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = UserRolesApi.Administrator)]
        public ActionResult Edit([Bind(Include = "ID,Name,ParentID")] RealEstateTypologyViewModels realEstateTypology)
        {
            if (!UnitOfWork.Authorizations.RealEstateTypology.allowEdit(realEstateTypology))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (ModelState.IsValid)
            {
                UnitOfWork.Repositories.RealEstateTypologies.Edit(realEstateTypology);
                UnitOfWork.Save();
                return RedirectToAction("Index");
            }

            ViewBag.ParentID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All().Where(a => a.ID != realEstateTypology.ID), "ID", "Name", realEstateTypology.ParentID);
            return View(realEstateTypology);
        }

        [Authorize(Roles = UserRolesApi.Administrator)]
        // GET: RealEstateTypologies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateTypologyViewModels realEstateTypology = UnitOfWork.Repositories.RealEstateTypologies.GetById((int)id);
            if (realEstateTypology == null)
            {
                return HttpNotFound();
            }

            if (!UnitOfWork.Authorizations.RealEstateTypology.allowDelete(realEstateTypology))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(realEstateTypology);
        }

        // POST: RealEstateTypologies/Delete/5
        [Authorize(Roles = UserRolesApi.Administrator)]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealEstateTypologyViewModels realEstateTypology = UnitOfWork.Repositories.RealEstateTypologies.GetById(id);
            if (realEstateTypology == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (!UnitOfWork.Authorizations.RealEstateTypology.allowDelete(realEstateTypology))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            UnitOfWork.Repositories.RealEstateTypologies.Remove(realEstateTypology);
            UnitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
