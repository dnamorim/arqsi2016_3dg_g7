﻿using System.Web.Mvc;

namespace ImoGestClient.Controllers
{
    public class PhotosController : MainController
    {

        // GET: /Photos/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = UnitOfWork.Repositories.RealEstates.GetPhotoById(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }
    }
}