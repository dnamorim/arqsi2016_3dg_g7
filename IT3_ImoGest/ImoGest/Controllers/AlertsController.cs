﻿using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Microsoft.AspNet.Identity;
using ImoGestClient.Models;
using ImoGestClient.ViewModels;

namespace ImoGestClient.Controllers
{
    [Authorize(Roles = UserRolesApi.Client)]
    public class AlertsController : MainController
    {
        // GET: Alerts
        public ActionResult Index()
        {
            if (!UnitOfWork.Authorizations.Alert.allowView())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return View(UnitOfWork.Repositories.Alerts.AllFromUser());
        }

        // GET: Alerts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlertViewModels alert = UnitOfWork.Repositories.Alerts.GetById((int)id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            if (!UnitOfWork.Authorizations.Alert.allowDetails(alert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(alert);
        }

        // GET: Alerts/Create
        public ActionResult Create()
        {
            if (!UnitOfWork.Authorizations.Alert.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            ViewBag.Field = UnitOfWork.Repositories.Alerts.GetAllFields();
            ViewBag.Constraint = UnitOfWork.Repositories.Alerts.GetAllConstraints();
            return View();
        }

        // POST: Alerts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AlertViewModels alert)
        {
            if (!UnitOfWork.Authorizations.Alert.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid)
            {
                if (alert.StartDateTime <= alert.EndDateTime)
                {
                    alert.OwnerID = User.Identity.GetUserId();
                    UnitOfWork.Repositories.Alerts.Add(alert);
                    UnitOfWork.Save();
                    return RedirectToAction("Index");
                }
            }

            return View(alert);
        }

        // GET: Alerts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlertViewModels alert = UnitOfWork.Repositories.Alerts.GetById((int)id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            if (!UnitOfWork.Authorizations.Alert.allowEdit(alert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            
            return View(alert);
        }

        // POST: Alerts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Message,StartDateTime,EndDateTime,OwnerID")] AlertViewModels alert)
        {
            if (!UnitOfWork.Authorizations.Alert.allowEdit(alert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid)
            {
                UnitOfWork.Repositories.Alerts.Edit(alert);
                UnitOfWork.Save();
                return RedirectToAction("Index");
            }
            return View(alert);
        }

        // GET: Alerts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AlertViewModels alert = UnitOfWork.Repositories.Alerts.GetById((int)id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            if (!UnitOfWork.Authorizations.Alert.allowDelete(alert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(alert);
        }

        // POST: Alerts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AlertViewModels alert = UnitOfWork.Repositories.Alerts.GetById(id);

            if (!UnitOfWork.Authorizations.Alert.allowDelete(alert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            UnitOfWork.Repositories.Alerts.Remove(alert);
            UnitOfWork.Save();

            return RedirectToAction("Index");
        }
    }
}
