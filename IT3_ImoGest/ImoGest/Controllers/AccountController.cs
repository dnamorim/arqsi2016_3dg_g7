﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using ImoGestClient.DataLayerAccess;
using ImoGestClient.Models;
using ImoGestClient.ViewModels;

namespace ImoGestClient.Controllers
{
    [Authorize]
    [RoutePrefix("Account")]
    public class AccountController : MainController
    {
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var clientToken = UnitOfWork.Client;
                HttpContent contentToken = new StringContent("grant_type=password&username=" + model.Email + "&password=" + model.Password, System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");
                var responseToken = await clientToken.PostAsync("Token", contentToken);
                if (responseToken.IsSuccessStatusCode)
                {
                    TokenApi tokenResponse = await responseToken.Content.ReadAsAsync<TokenApi>();
                    UserApi user = new UserApi();
                    user.Token = tokenResponse;

                    var clientUserInfo = UnitOfWork.Client;
                    clientUserInfo.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", user.Token.AccessToken);

                    HttpResponseMessage responseUserInfo = await clientUserInfo.GetAsync("api/Account/UserInfo");
                    if (responseUserInfo.IsSuccessStatusCode)
                    {
                        string contentUserInfo = await responseUserInfo.Content.ReadAsStringAsync();

                        var resultsUserInfo = JsonConvert.DeserializeObject<dynamic>(contentUserInfo);

                        user.Id = resultsUserInfo.Id;
                        user.Roles = new List<String>(((string)resultsUserInfo.Roles).Split(','));

                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                            1,
                            model.Email,
                            DateTime.Now,
                            user.Token.ExpiresAt,
                            model.RememberMe,
                            JsonConvert.SerializeObject(user));

                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName + "_bearer", encTicket);
                        Response.Cookies.Add(faCookie);
                        return RedirectToLocal(returnUrl);
                    }
                }
            }
            return View(model);
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult RemoteRegister()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var client = UnitOfWork.Client;
                string json = JsonConvert.SerializeObject(model, UnitOfWork.SerializerSettings);
                HttpContent content = new StringContent(json, System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Account/Register", content);
                if (response.IsSuccessStatusCode)
                {
                    return await Login(new LoginViewModel() { Email = model.Email, Password = model.Password, RememberMe = false }, "");
                }
            }
            return View(model);
        }

        // POST: /Account/Remote/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Remote/LogOff")]
        public async Task<ActionResult> LogOff()
        {

            if (UnitOfWork.Authorizations.ApplicationUser.AuthenticationType == "bearer")
            {

                HttpClient client = UnitOfWork.Client;
                HttpContent contentToken = new StringContent("", System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");
                HttpResponseMessage response = await client.PostAsync("api/Account/Logout", contentToken);
                if (response.IsSuccessStatusCode)
                {
                    HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName + "_bearer"];
                    if (authCookie != null)
                    {
                        authCookie.Value = String.Empty;
                        authCookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(authCookie);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}