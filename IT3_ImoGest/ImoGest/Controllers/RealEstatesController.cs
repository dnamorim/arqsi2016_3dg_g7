﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Infrastructure;
using Microsoft.AspNet.Identity;
using ImoGestClient.Models;
using ImoGestClient.ViewModels;

namespace ImoGestClient.Controllers
{
    [Authorize(Roles = UserRolesApi.Client)]
    public class RealEstatesController : MainController
    {
        // GET: RealEstates
        public ActionResult Index()
        {
            if (!UnitOfWork.Authorizations.RealEstate.allowView())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(UnitOfWork.Repositories.RealEstates.All());
        }

        // GET: RealEstates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateViewModels realEstate = UnitOfWork.Repositories.RealEstates.GetById((int)id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            if (!UnitOfWork.Authorizations.RealEstate.allowDetails(realEstate))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(realEstate);
        }

        // GET: RealEstates/Create
        public ActionResult Create()
        {
            if (!UnitOfWork.Authorizations.RealEstate.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.TypologyID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All(), "ID", "FullDescription").ToList();
            return View();
        }

        // POST: RealEstates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "ID,Description,TypologyID,Area")] RealEstateViewModels realEstate, 
            [Bind(Include = "ID,Latitude,Longitude")] LocalizationViewModels localization, 
            IEnumerable<HttpPostedFileBase> uploads
            ) {

            if (!UnitOfWork.Authorizations.RealEstate.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    realEstate.Photos = new List<PhotoViewModels>();

                    foreach (var upload in uploads)
                    {
                        if (upload != null && upload.ContentLength > 0)
                        {
                            var photo = new PhotoViewModels
                            {
                                PhotoName = System.IO.Path.GetFileName(upload.FileName),
                                ContentType = upload.ContentType
                            };
                            using (var reader = new System.IO.BinaryReader(upload.InputStream))
                            {
                                photo.Content = reader.ReadBytes(upload.ContentLength);
                            }
                            realEstate.Photos.Add(photo);
                        }
                    }

                    localization.RealEstate = realEstate;
                    realEstate.Localization = localization;
                    realEstate.OwnerID = User.Identity.GetUserId();

                    UnitOfWork.Repositories.RealEstates.Add(realEstate);
                    UnitOfWork.Save();

                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            ViewBag.TypologyID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All(), "ID", "FullDescription");
            return View(realEstate);
        }

        // GET: RealEstates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateViewModels realEstate = UnitOfWork.Repositories.RealEstates.GetById((int) id);

            if (realEstate == null)
            {
                return HttpNotFound();
            }

            if (!UnitOfWork.Authorizations.RealEstate.allowEdit(realEstate))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            
            ViewBag.TypologyID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All(), "ID", "FullDescription");
            return View(realEstate);
        }

        
        // POST: RealEstates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Description,TypologyID,Area,OwnerID")] RealEstateViewModels realEstate,
            [Bind(Include = "Latitude,Longitude")] LocalizationViewModels localization,
            IEnumerable<HttpPostedFileBase> uploads)
        {
            if (!UnitOfWork.Authorizations.RealEstate.allowEdit(realEstate))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var realEstateOnDB = UnitOfWork.Repositories.RealEstates.GetById(realEstate.ID);

                    if (uploads?.FirstOrDefault() != null)
                    {
                        UnitOfWork.Repositories.RealEstates.Remove(realEstateOnDB);
                        UnitOfWork.Save();

                        realEstateOnDB.Photos = new List<PhotoViewModels>();

                        foreach (var upload in uploads)
                        {
                            if (upload != null && upload.ContentLength > 0)
                            {
                                var photo = new PhotoViewModels
                                {
                                    PhotoName = System.IO.Path.GetFileName(upload.FileName),
                                    ContentType = upload.ContentType
                                };
                                using (var reader = new System.IO.BinaryReader(upload.InputStream))
                                {
                                    photo.Content = reader.ReadBytes(upload.ContentLength);
                                }
                                realEstateOnDB.Photos.Add(photo);
                            }
                        }

                        UnitOfWork.Repositories.RealEstates.Add(realEstateOnDB);
                        UnitOfWork.Save();
                    } else
                    {
                        realEstateOnDB.Area = realEstate.Area;
                        realEstateOnDB.Description = realEstate.Description;
                        realEstateOnDB.Localization.Latitude = localization.Latitude;
                        realEstateOnDB.Localization.Longitude = localization.Longitude;
                        realEstateOnDB.Typology = realEstate.Typology;
                        UnitOfWork.Repositories.RealEstates.Edit(realEstateOnDB);
                    }

                    return RedirectToAction("Index");
                }

                ViewBag.TypologyID = new SelectList(UnitOfWork.Repositories.RealEstateTypologies.All(), "ID", "FullDescription");
                return View(realEstate);
            }
            catch(RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }
            return View(realEstate);
        }

        // GET: RealEstates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateViewModels realEstate = UnitOfWork.Repositories.RealEstates.GetById((int)id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }

            if (!UnitOfWork.Authorizations.RealEstate.allowDelete(realEstate))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(realEstate);
        }

        // POST: RealEstates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealEstateViewModels realEstate = UnitOfWork.Repositories.RealEstates.GetById(id);
            if (realEstate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (!UnitOfWork.Authorizations.RealEstate.allowDelete(realEstate))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            UnitOfWork.Repositories.RealEstates.Remove(realEstate);
            UnitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
