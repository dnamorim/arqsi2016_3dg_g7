﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ImoGestClient.Models;
using ImoGestClient.ViewModels;

namespace ImoGestClient.Controllers
{
    [Authorize(Roles = UserRolesApi.Client)]
    public class AdvertsController : MainController
    {
        // GET: Adverts
        public ActionResult Index()
        {
            if (!UnitOfWork.Authorizations.Advert.allowView())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            return View(UnitOfWork.Repositories.Adverts.All());
        }

        // GET: Adverts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertViewModels advert = UnitOfWork.Repositories.Adverts.GetById((int)id);
            if (advert == null)
            {
                return HttpNotFound();
            }

            if (!UnitOfWork.Authorizations.Advert.allowDetails(advert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return View(advert);
        }

        // GET: Adverts/Create
        public ActionResult Create()
        {
            if (!UnitOfWork.Authorizations.Advert.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            ViewBag.RealEstateID = new SelectList(UnitOfWork.Repositories.RealEstates.AllFromUser(), "ID", "Description");
            ViewBag.TypeID = new SelectList(UnitOfWork.Repositories.AdvertTypes.All(), "ID", "Name").ToList();
            return View();
        }

        // POST: Adverts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Price,RealEstateID,TypeID")] AdvertViewModels advert)
        {
            if (!UnitOfWork.Authorizations.Advert.allowAdd())
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid)
            {
                advert.DateCreation = DateTime.Now;
                advert.DateLastUpdate = DateTime.Now;
                advert.OwnerID = User.Identity.GetUserId();
                UnitOfWork.Repositories.Adverts.Add(advert);
                UnitOfWork.Save();
                return RedirectToAction("Index");
            }

            ViewBag.RealEstateID = new SelectList(UnitOfWork.Repositories.RealEstates.AllFromUser(), "ID", "Description");
            ViewBag.TypeID = new SelectList(UnitOfWork.Repositories.AdvertTypes.All(), "ID", "Name");
            return View(advert);
        }

        // GET: Adverts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdvertViewModels advert = UnitOfWork.Repositories.Adverts.GetById((int)id);

            if (advert == null)
            {
                return HttpNotFound();
            }

            if (!UnitOfWork.Authorizations.Advert.allowEdit(advert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            ViewBag.RealEstateID = new SelectList(UnitOfWork.Repositories.RealEstates.AllFromUser(), "ID", "Description");
            ViewBag.TypeID = new SelectList(UnitOfWork.Repositories.AdvertTypes.All(), "ID", "Name");
            return View(advert);
        }

        // POST: Adverts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Price,RealEstateID,TypeID,OwnerID")] AdvertViewModels advert)
        {
            if (!UnitOfWork.Authorizations.Advert.allowEdit(advert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid)
            {
                advert.DateLastUpdate = DateTime.Now;
                UnitOfWork.Repositories.Adverts.Edit(advert);
                UnitOfWork.Save();
                return RedirectToAction("Index");
            }
            ViewBag.RealEstateID = new SelectList(UnitOfWork.Repositories.RealEstates.AllFromUser(), "ID", "Description");
            ViewBag.TypeID = new SelectList(UnitOfWork.Repositories.AdvertTypes.All(), "ID", "Name");
            return View(advert);
        }

        // GET: Adverts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AdvertViewModels advert = UnitOfWork.Repositories.Adverts.GetById((int)id);

            if (advert == null)
            {
                return HttpNotFound();
            }

            if (!UnitOfWork.Authorizations.Advert.allowDelete(advert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return View(advert);
        }

        // POST: Adverts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AdvertViewModels advert = UnitOfWork.Repositories.Adverts.GetById(id);
            if (!UnitOfWork.Authorizations.Advert.allowDelete(advert))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            UnitOfWork.Repositories.Adverts.Remove(advert);
            UnitOfWork.Save();
            return RedirectToAction("Index");
        }
    }
}
