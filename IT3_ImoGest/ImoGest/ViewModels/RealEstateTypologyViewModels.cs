﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ImoGestClient.ViewModels
{
    public class RealEstateTypologyViewModels
    {
        public int ID { get; set; }

        [Required, Display(Name = "Tipo"), StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [ForeignKey("Parent"), Display(Name = "Sub-Tipo de")]
        public int? ParentID { get; set; }
        public virtual RealEstateTypologyViewModels Parent { get; set; }

        [NotMapped, Display(Name = "Descrição Completa")]
        public string FullDescription { get; set; }
    }
}