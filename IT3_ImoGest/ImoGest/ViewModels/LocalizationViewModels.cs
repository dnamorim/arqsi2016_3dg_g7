﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGestClient.ViewModels
{
    public class LocalizationViewModels
    {
        [Key, ForeignKey("RealEstate")]
        public int RealEstateID { get; set; }
        public virtual RealEstateViewModels RealEstate { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }

        [NotMapped]
        public string Coordinates { get; set; }
        
    }
}