﻿using System.ComponentModel.DataAnnotations;

namespace ImoGestClient.ViewModels
{
    public class PhotoViewModels
    {
        public int PhotoID { get; set; }
        [StringLength(255)]
        public string PhotoName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }

        public int RealEstateID { get; set; }
        public virtual RealEstateViewModels RealEstate { get; set; }
    }
}