﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGestClient.ViewModels
{
    public class RealEstateViewModels
    {
        public int ID { get; set; }
        [Display(Name = "Descrição do Imóvel"), Required, StringLength(120, MinimumLength = 8)]
        public string Description { get; set; }

        [Display(Name = "Localização")]
        public virtual LocalizationViewModels Localization { get; set; }

        [Display(Name = "Tipo de Imóvel")]
        public int TypologyID { get; set; }
        public virtual RealEstateTypologyViewModels Typology { get; set; }

        [Display(Name = "Área"), Required]
        [Range(0.0, Double.MaxValue, ErrorMessage = "Invalid area value")]
        public double Area { get; set; }

        [Display(Name = "Fotografias")]
        public virtual ICollection<PhotoViewModels> Photos { get; set; }

        [ForeignKey("Owner")]
        public string OwnerID { get; set; }
        public virtual ApplicationUserViewModels Owner { get; set; }
    }
}