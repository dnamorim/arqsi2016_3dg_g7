﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ImoGestClient.ViewModels
{
    public class AdvertViewModels
    {
        [Display(Name = "Anúncio")]
        public int ID { get; set; }

        [Required, Display(Name = "Título do Anúncio")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Tipo de Anúncio")]
        public int TypeID { get; set; }
        public virtual AdvertTypeViewModels Type { get; set; }

        [Display(Name = "Imóvel")]
        public int RealEstateID { get; set; }
        public virtual RealEstateViewModels RealEstate { get; set; }

        [Required, Display(Name = "Preço"), DataType(DataType.Currency), DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        [ForeignKey("Owner")]
        public string OwnerID { get; set; }
        public virtual ApplicationUserViewModels Owner { get; set; }

        [Display(Name = "Data de Criação")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateCreation { get; set; }

        [Display(Name = "Data da Última Actualização")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateLastUpdate { get; set; }
        
        [ForeignKey("Mediator")]
        [Display(Name = "Mediador")]
        public string MediatorID { get; set; }
        public virtual ApplicationUserViewModels Mediator { get; set; }

        [Display(Name = "Validação do Mediador")]
        public bool? MediatorValidation { get; set; }
    }
}