﻿using System.ComponentModel.DataAnnotations;

namespace ImoGestClient.ViewModels
{
    public class AdvertTypeViewModels
    {
        [Display(Name = "Tipo de Anúncio")]
        public int ID { get; set; }
        public string Name { get; set; }
    }
}