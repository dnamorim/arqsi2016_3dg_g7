﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGestClient.ViewModels
{
        public class AlertParameterViewModels
        {
            public int ID { get; set; }
            [Required]
            public FieldViewModels Field { get; set; }
            [Required]
            public ConstraintViewModels Constraint { get; set; }
            [Required, StringLength(100, MinimumLength = 2)]
            public string Value { get; set; }

            [ForeignKey("Alert")]
            public int AlertID { get; set; }
            public virtual AlertViewModels Alert { get; set; }
        }

        public class AlertParameterFieldViewModels
        {
            [Column(Order = 0), Key]
            public FieldViewModels Field { get; set; }
            [Column(Order = 1), Key]
            public ConstraintViewModels Constraint { get; set; }
        }

        public enum FieldViewModels
        {
            [Display(Name = "Tipo de Imóvel")]
            RealEstateTypology = 1,

            [Display(Name = "Tipo de Anúncio")]
            AdvertType = 2,

            [Display(Name = "Área")]
            Area = 3,

            [Display(Name = "Preço")]
            Price = 4,

        }

        public enum ConstraintViewModels
        {
            [Display(Name = "Igual a (=)")]
            Equals = 1,

            [Display(Name = "Menor que (<)")]
            Less = 2,

            [Display(Name = "Maior que (>)")]
            More = 3
        }
}