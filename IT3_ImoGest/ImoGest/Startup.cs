﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImoGestClient.Startup))]
namespace ImoGestClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
