﻿using System;
using System.Security.Principal;
using ImoGestClassLibrary.DataAccessLayer;
using System.Web;

namespace ImoGestClassLibrary.DataAccessLayer
{
    // ADAPTED FROM: http://www.asp.net/mvc/overview/older-versions/getting-started-with-ef-5-using-mvc-4/implementing-the-repository-and-unit-of-work-patterns-in-an-asp-net-mvc-application
    public class UnitOfWork : IDisposable
    {
        private ImoGestContext _context = new ImoGestContext();
        public ImoGestContext Context
        {
            get
            {
                return _context ?? (_context = new ImoGestContext());
            }
        }
        
        public Repositories _repositories;
        public Repositories Repositories
        {
            get
            {
                return _repositories ?? (_repositories = Repositories.Instance(this));
            }
        }

        private IPrincipal _user;
        public IPrincipal User
        {
            get
            {
                return _user ?? (_user = HttpContext.Current.User);
            }
        }

        private Authorization _authorizations;
        public Authorization Authorizations
        {
            get
            {
                return _authorizations ?? (_authorizations = Authorization.Instance(this));
            }
        }

        private bool disposed = false;

        private UnitOfWork(ImoGestContext context, IPrincipal user)
        {
            this._context = context;
            this._user = user;
        }

        public void Save()
        {
            this.Context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public static UnitOfWork Instance(ImoGestContext context = null, IPrincipal user = null)
        {
            return new UnitOfWork(context, user);
        }
    }
}
