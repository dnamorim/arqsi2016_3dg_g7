﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using ImoGestClassLibrary.Models;
using System.Web.Mvc.Html;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class AlertRepository : GenericRepository<Alert>
    {
        private UnitOfWork unitOfWork;

        public AlertRepository(UnitOfWork unitOfWork) : base(unitOfWork.Context)
        {
            this.unitOfWork = unitOfWork;
        }

        public Alert Add(Alert alert)
        {
            base.Insert(alert);
            return alert;
        }

        public Alert Edit(Alert alert)
        {
            base.Update(alert);
            return alert;
        }

        public virtual Alert Remove(Alert alert)
        {
            base.Delete(alert);
            return alert;
        }

        public bool Exists(int Id)
        {
            return context.Alerts.Count(a => a.ID == Id) > 0;
        }

        public ICollection<Alert> All()
        {
            return context.Alerts.ToList();
        }

        public virtual Alert Get(int Id)
        {
            return context.Alerts.Find(Id);
        }

        public ICollection<SelectListItem> GetAlertFields()
        {
            return EnumHelper.GetSelectList(typeof(Field));
        }

        public ICollection<SelectListItem> GetAlertConstraints()
        {
            return EnumHelper.GetSelectList(typeof(ImoGestClassLibrary.Models.Constraint));
        }
        
        public ICollection<Alert> AllFromUser()
        {
            var userId = unitOfWork.Authorizations.ApplicationUser.UserId;
            return context.Alerts.Where(a => a.OwnerID == userId).ToList();
        }
    }
}
