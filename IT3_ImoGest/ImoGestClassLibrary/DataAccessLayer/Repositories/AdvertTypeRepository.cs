﻿using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class AdvertTypeRepository : GenericRepository<AdvertType>
    {
        private UnitOfWork unitOfWork;

        public AdvertTypeRepository(UnitOfWork unitOfWork) : base(unitOfWork.Context)
        {
            this.unitOfWork = unitOfWork;
        }

        public ICollection<AdvertType> All()
        {
            return context.AdvertTypes.ToList();
        }
    }
}
