﻿using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class ApplicationUserRepository : GenericRepository<ApplicationUser>
    {
        private UnitOfWork unitOfWork;

        public ApplicationUserRepository(UnitOfWork unitOfWork) : base(unitOfWork.Context)
        {
            this.unitOfWork = unitOfWork;
        }

        public ICollection<ApplicationUser> GetAllMediators()
        {
            string idRole = context.Roles.Single(r => r.Name == UserRoles.Mediator).Id;
            return context.Users.Where(u => u.Roles.Any(r => r.RoleId == idRole)).ToList();
        }
    }
}
