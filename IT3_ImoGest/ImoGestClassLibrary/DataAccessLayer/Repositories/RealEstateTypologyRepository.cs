﻿using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class RealEstateTypologyRepository : GenericRepository<RealEstateTypology>
    {
        private UnitOfWork unitOfWork;

        public RealEstateTypologyRepository(UnitOfWork unitOfWork) : base(unitOfWork.Context) {
            this.unitOfWork = unitOfWork;
        }

        public RealEstateTypology Add(RealEstateTypology realEstateTypology)
        {
            base.Insert(realEstateTypology);
            return realEstateTypology;
        }

        public RealEstateTypology Edit(RealEstateTypology realEstateTypology)
        {
            base.Update(realEstateTypology);
            return realEstateTypology;
        }

        public RealEstateTypology Remove(RealEstateTypology realEstatTypology)
        {
            base.Delete(realEstatTypology);
            return realEstatTypology;
        }

        public bool Exists(int Id)
        {
            return context.RealEstateTypologies.Count(ret => ret.ID == Id) > 0;
        }

        public ICollection<RealEstateTypology> All()
        {
            return context.RealEstateTypologies.ToList();
        }

        public virtual RealEstateTypology GetById(int Id)
        {
            return context.RealEstateTypologies.Find(Id);
        }

        public bool IsUnique(RealEstateTypology realEstateTypology)
        {
            return !context.RealEstateTypologies.Any(
                ret => ret.Name == realEstateTypology.Name && ret.Parent == realEstateTypology.Parent);
        }

        public bool IsParent(RealEstateTypology realEstateTypology)
        {
            return context.RealEstateTypologies.Any(ret => ret.ParentID == realEstateTypology.ID);
        }

        public bool IsBeingUsed(RealEstateTypology realEstateTypology)
        {
            return context.RealEstates.Any(re => re.TypologyID == realEstateTypology.ID);
        }
     }
}
