﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    // Agregator Class for all Repositories in the Class Library
    public class Repositories
    {
        private readonly UnitOfWork unitOfWork;
        private Repositories(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        private RealEstateTypologyRepository _realEstateTypologies;
        public RealEstateTypologyRepository RealEstateTypologies
        {
            get
            {
                return _realEstateTypologies ?? (_realEstateTypologies = new RealEstateTypologyRepository(unitOfWork));
            }
        }

        private AdvertTypeRepository _advertTypes;
        public AdvertTypeRepository AdvertTypes
        {
            get
            {
                return _advertTypes ?? (_advertTypes = new AdvertTypeRepository(unitOfWork));
            }
        }

        private AdvertRepository _adverts;
        public AdvertRepository Adverts
        {
            get
            {
                return _adverts ?? (_adverts = new AdvertRepository(unitOfWork));
            }
        }

        private RealEstateRepository _realEstates;
        public RealEstateRepository RealEstates
        {
            get
            {
                return _realEstates ?? (_realEstates = new RealEstateRepository(unitOfWork));
            }
        }

        private AlertRepository _alerts;
        public AlertRepository Alerts
        {
            get
            {
                return _alerts ?? (_alerts = new AlertRepository(unitOfWork));
            }
        }

        private ApplicationUserRepository _applicationUsers;
        public ApplicationUserRepository ApplicationUsers
        {
            get
            {
                return _applicationUsers ?? (_applicationUsers = new ApplicationUserRepository(unitOfWork));
            }
        }

        
        public static Repositories Instance(UnitOfWork unitOfWork)
        {
            return new Repositories(unitOfWork);
        }

    }
}
