﻿using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class AdvertRepository : GenericRepository<Advert>
    {
        private UnitOfWork _unitOfWork;

        public AdvertRepository(UnitOfWork unitOfWork) : base(unitOfWork.Context)
        {
            this._unitOfWork = unitOfWork;
        }

        public Advert Add(Advert advert)
        {
            base.Insert(advert);
            return advert;
        }

        public Advert Edit(Advert advert) {
            base.Update(advert);
            return advert;
        }

        public Advert Remove(Advert advert)
        {
            base.Delete(advert);
            return advert;
        }

        public bool Exists(int Id)
        {
            return context.Adverts.Count(ad => ad.ID == Id) > 0;
        }

        public ICollection<Advert> All()
        {
            return context.Adverts.ToList();
        }

        public virtual Advert Get(int Id)
        {
            return context.Adverts.Find(Id);
        }

        public ICollection<Advert> Approved()
        {
            return context.Adverts.Where(ad => ad.MediatorValidation == true).ToList();
        }
    }
}
