﻿using ImoGestClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class RealEstateRepository : GenericRepository<RealEstate>
    {
        private UnitOfWork unitOfWork;

        public RealEstateRepository(UnitOfWork unitOfWork) : base(unitOfWork.Context)
        {
            this.unitOfWork = unitOfWork;
        }

        public RealEstate Add(RealEstate realEstate)
        {
            base.Insert(realEstate);
            return realEstate;
        }

        public RealEstate Edit(RealEstate realEstate)
        {
            base.Update(realEstate);
            return realEstate;
        }

        public RealEstate Remove(RealEstate realEstate)
        {
            RemovePhotos(realEstate);
            base.Delete(realEstate);
            return realEstate;
        }

        public RealEstate RemovePhotos(RealEstate realEstate)
        {
            // Delete Photos before remove the RealEstate
            List<int> lstPhotosToRemove = context.RealEstatePhotos.AsNoTracking().Where(rep => rep.RealEstateID == realEstate.ID).Select(p => p.PhotoID).ToList();
            foreach (var idPhoto in lstPhotosToRemove)
            {
                context.RealEstatePhotos.Remove(context.RealEstatePhotos.Find(idPhoto));
            }

            return realEstate;
        }

        public bool Exists(int Id)
        {
            return context.RealEstates.Count(re => re.ID == Id) > 0;
        }

        public ICollection<RealEstate> All()
        {
            return context.RealEstates.ToList();
        }

        public virtual RealEstate Get(int Id)
        {
            return context.RealEstates.Find(Id);
        }

        public ICollection<RealEstate> AllRealEstatesFromUser()
        {
            var userId = unitOfWork.Authorizations.ApplicationUser.UserId;
            return context.RealEstates.Where(a => a.OwnerID == userId).ToList();
        }

        public bool IsInAdverts(RealEstate realEstate)
        {
            return context.Adverts.Any(adv => adv.RealEstateID == realEstate.ID);
        }

        public Photo GetPhotoById(int id)
        {
            return context.RealEstatePhotos.Find(id);
        }
    }
}
