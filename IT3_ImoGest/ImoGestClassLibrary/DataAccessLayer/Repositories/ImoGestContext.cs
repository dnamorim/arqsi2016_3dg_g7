﻿using ImoGestClassLibrary.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class ImoGestContext : ApplicationDbContext
    {
        public ImoGestContext() : base() { }

        public DbSet<RealEstateTypology> RealEstateTypologies { get; set; }
        public DbSet<AdvertType> AdvertTypes { get; set; }
        public DbSet<RealEstate> RealEstates { get; set; }
        public DbSet<Advert> Adverts { get; set; }
        public DbSet<Photo> RealEstatePhotos { get; set; }
        public DbSet<Localization> Localizations { get; set; }
        public DbSet<Alert> Alerts { get; set; }
        public DbSet<AlertParameterField> AlertParameterFields { get; set; }
        public DbSet<AlertParameter> AlertParameters { get; set; }
    }
}

namespace ImoGestClassLibrary.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<RealEstate>().HasOptional(r => r.Localization).WithRequired(l => l.RealEstate).WillCascadeOnDelete(true);
            modelBuilder.Entity<RealEstate>().HasMany(re => re.Photos).WithRequired(rep => rep.RealEstate).WillCascadeOnDelete(true);
            modelBuilder.Entity<Alert>().HasMany(a => a.Parameters).WithRequired(p => p.Alert).WillCascadeOnDelete(true);
        }
    }
}