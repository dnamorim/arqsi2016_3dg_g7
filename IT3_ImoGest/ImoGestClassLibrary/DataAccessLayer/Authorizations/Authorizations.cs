﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class Authorization
    {
        private UnitOfWork unitOfWork;

        private Authorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        private ApplicationUserAuthorization _applicationUser;
        public ApplicationUserAuthorization ApplicationUser {
            get
            {
                return _applicationUser ?? (_applicationUser = new ApplicationUserAuthorization(unitOfWork));
            }
        }

        private AdvertAuthorization _advertAuthorization;
        public AdvertAuthorization Advert
        {
            get
            {
                return _advertAuthorization ?? (_advertAuthorization = new AdvertAuthorization(unitOfWork));
            }
        }

        private RealEstateAuthorization _realEstate;
        public RealEstateAuthorization RealEstate
        {
            get
            {
                return _realEstate ?? (_realEstate = new RealEstateAuthorization(unitOfWork));
            }
        }

        private RealEstateTypologyAuthorization _realEstateTypology;
        public RealEstateTypologyAuthorization RealEstateTypology
        {
            get
            {
                return _realEstateTypology ?? (_realEstateTypology = new RealEstateTypologyAuthorization(unitOfWork));
            }
        }

        private AlertAuthorization _alert;
        public AlertAuthorization Alert
        {
            get
            {
                return _alert ?? (_alert = new AlertAuthorization(unitOfWork));
            }
        }

        public static Authorization Instance(UnitOfWork unitOfWork)
        {
            return new Authorization(unitOfWork);
        }
    }
}
