﻿using System;
using ImoGestClassLibrary.Models;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class RealEstateAuthorization
    {
        private UnitOfWork unitOfWork;
        public RealEstateAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool allowView()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool allowDelete(RealEstate realEstate)
        {
            return (realEstate.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId && notInAdverts(realEstate));
        }

        private bool notInAdverts(RealEstate realEstate)
        {
            return !(unitOfWork.Repositories.RealEstates.IsInAdverts(realEstate));
        }

        public bool allowEdit(RealEstate realEstate)
        {
            return (realEstate.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool allowDetails(RealEstate realEstate)
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool isOwner(RealEstate realEstate)
        {
            return (realEstate.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }
    }
}
