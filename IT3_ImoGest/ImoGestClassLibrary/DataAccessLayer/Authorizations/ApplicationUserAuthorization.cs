﻿using Microsoft.AspNet.Identity;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class ApplicationUserAuthorization
    {
        private UnitOfWork _unitOfWork;
        public ApplicationUserAuthorization(UnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public bool IsAuthenticated
        {
            get
            {
                return ((_unitOfWork.User?.Identity != null) && (_unitOfWork.User.Identity.IsAuthenticated));
            }
        }

        public string UserId
        {
            get
            {
                return (this.IsAuthenticated) ? (_unitOfWork.User.Identity.GetUserId()) : "";
            }
        }

        public string UserName
        {
            get
            {
                return (this.IsAuthenticated) ? (_unitOfWork.User.Identity.GetUserName()) : "";
            }
        }

        public string AuthenticationType
        {
            get
            {
                return (this.IsAuthenticated) ? (_unitOfWork.User.Identity.AuthenticationType) : "";
            }
        }
        public bool IsInRole(string role)
        {
            return this.IsAuthenticated && (_unitOfWork.User.IsInRole(role));
        }

    }
}
