﻿using ImoGestClassLibrary.Models;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class AlertAuthorization
    {
        private UnitOfWork unitOfWork;
        public AlertAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool allowView()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool allowDelete(Alert alert)
        {
            return (alert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool allowEdit(Alert alert)
        {
            return (alert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool allowDetails(Alert alert)
        {
            return (alert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool isOwner(Alert alert)
        {
            return (alert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }
    }
}
