﻿using ImoGestClassLibrary.Models;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class RealEstateTypologyAuthorization
    {
        private UnitOfWork unitOfWork;

        public RealEstateTypologyAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Administrator);
        }

        public bool allowView()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsAuthenticated;
        }

        public bool allowDelete(RealEstateTypology type)
        {
            return (unitOfWork.Authorizations.ApplicationUser.IsAuthenticated &&
                unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Administrator) &&
                notParentTypeOf(type) && notAppliedInRealEstates(type)); 
        }

        public bool allowEdit(RealEstateTypology type)
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Administrator);
        }

        public bool allowDetails(RealEstateTypology type)
        {
            return unitOfWork.Authorizations.ApplicationUser.IsAuthenticated;
        }

        private bool notParentTypeOf(RealEstateTypology type)
        {
            return !(unitOfWork.Repositories.RealEstateTypologies.IsParent(type));
        }

        private bool notAppliedInRealEstates(RealEstateTypology type)
        {
            return !(unitOfWork.Repositories.RealEstateTypologies.IsBeingUsed(type));
        }

    }
}
