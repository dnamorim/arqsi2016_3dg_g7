﻿using ImoGestClassLibrary.Models;

namespace ImoGestClassLibrary.DataAccessLayer
{
    public class AdvertAuthorization
    {
        private UnitOfWork unitOfWork;
        public AdvertAuthorization(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool allowAdd()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool allowView()
        {
            return unitOfWork.Authorizations.ApplicationUser.IsInRole(UserRoles.Client);
        }

        public bool allowDelete(Advert advert)
        {
            return (advert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool allowEdit(Advert advert)
        {
            return (advert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool allowDetails(Advert advert)
        {
            return (advert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }

        public bool isOwner(Advert advert)
        {
            return (advert.OwnerID == unitOfWork.Authorizations.ApplicationUser.UserId);
        }
    }
}
