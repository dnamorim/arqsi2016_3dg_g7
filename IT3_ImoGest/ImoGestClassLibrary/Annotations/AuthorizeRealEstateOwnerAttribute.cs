﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ImoGestClassLibrary.Models;
using ImoGestClassLibrary.DataAccessLayer;

namespace ImoGestClassLibrary.Annotations
{
    public class AuthorizeRealEstateOwnerAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                return false;
            }

            var rd = httpContext.Request.RequestContext.RouteData;

            int id = int.Parse(rd.Values["ID"].ToString());
            var userID = httpContext.User.Identity.GetUserId();

            RealEstate re = new ImoGestContext().RealEstates.Where(r => r.ID == id).FirstOrDefault();

            return re.OwnerID == userID;
        }
    }
}