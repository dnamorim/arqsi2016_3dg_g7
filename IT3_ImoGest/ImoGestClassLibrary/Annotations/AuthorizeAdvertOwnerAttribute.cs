﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ImoGestClassLibrary.Models;
using ImoGestClassLibrary.DataAccessLayer;

namespace ImoGestClassLibrary.Annotations
{
    public class AuthorizeAdvertOwnerAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = base.AuthorizeCore(httpContext);
            if (!authorized)
            {
                return false;
            }

            var rd = httpContext.Request.RequestContext.RouteData;

            int id = int.Parse(rd.Values["ID"].ToString());
            var userID = httpContext.User.Identity.GetUserId();

            Advert ad = new ImoGestContext().Adverts.Where(r => r.ID == id).FirstOrDefault();

            return ad.OwnerID == userID;
        }
    }
}