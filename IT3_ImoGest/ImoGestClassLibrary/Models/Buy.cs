﻿namespace ImoGestClassLibrary.Models
{
    public class Buy : AdvertType
    { 
        public override string Name
        {
            get
            {
                return "Compra";
            }
        }
    }
}