﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImoGestClassLibrary.Models
{
    public class Localization
    {
        [Key, ForeignKey("RealEstate")]
        public int RealEstateID { get; set; }

        [Required]
        public double Latitude { get; set; }

        [Required]
        public double Longitude { get; set; }

        [NotMapped]
        public string Coordinates
        {
            get {
                return $"Lat: {this.Latitude}; Lon: {this.Longitude}";
            }
        }
        public virtual RealEstate RealEstate { get; set; }
    }
}