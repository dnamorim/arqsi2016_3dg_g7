﻿namespace ImoGestClassLibrary.Models
{
    public class Rent : AdvertType
    {
        public override string Name
        {
            get
            {
                return "Aluguer";
            }
        }
    }
}