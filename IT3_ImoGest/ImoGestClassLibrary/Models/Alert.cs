﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ImoGestClassLibrary.Models
{
    public class Alert
    {
        public int ID { get; set; }
        [Display(Name = "Nome Alerta"), StringLength(100, MinimumLength = 5)]
        public string Name { get; set; }
        [Display(Name = "Mensagem"), StringLength(int.MaxValue, MinimumLength = 2)]
        public string Message { get; set; }

        [Display(Name = "Data Início")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime StartDateTime { get; set; }

        [Display(Name = "Data Fim")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime EndDateTime { get; set; }

        [ForeignKey("Owner")]
        public string OwnerID { get; set; }
        public ApplicationUser Owner { get; set; }

        [Display(Name = "Condições")]
        public virtual ICollection<AlertParameter> Parameters { get; set; }
    }
}