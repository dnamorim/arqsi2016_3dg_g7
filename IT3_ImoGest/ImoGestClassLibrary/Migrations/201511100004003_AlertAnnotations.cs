namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlertAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AlertParameters", "Value", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AlertParameters", "Value", c => c.String());
        }
    }
}
