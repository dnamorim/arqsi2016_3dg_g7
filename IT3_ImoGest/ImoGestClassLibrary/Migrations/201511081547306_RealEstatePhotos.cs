namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstatePhotos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        PhotoID = c.Int(nullable: false, identity: true),
                        PhotoName = c.String(maxLength: 255),
                        Content = c.Binary(),
                        RealEstateID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PhotoID)
                .ForeignKey("dbo.RealEstates", t => t.RealEstateID, cascadeDelete: true)
                .Index(t => t.RealEstateID);
            
            AlterColumn("dbo.RealEstates", "Description", c => c.String(nullable: false, maxLength: 120));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Photos", "RealEstateID", "dbo.RealEstates");
            DropIndex("dbo.Photos", new[] { "RealEstateID" });
            AlterColumn("dbo.RealEstates", "Description", c => c.String(nullable: false));
            DropTable("dbo.Photos");
        }
    }
}
