namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstates : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RealEstates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        LocalizationID = c.Int(nullable: false),
                        TypeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RealEstateTypologies", t => t.TypeID, cascadeDelete: true)
                .Index(t => t.TypeID);
            
            CreateTable(
                "dbo.Localizations",
                c => new
                    {
                        RealEstateID = c.Int(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Altitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.RealEstateID)
                .ForeignKey("dbo.RealEstates", t => t.RealEstateID)
                .Index(t => t.RealEstateID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RealEstates", "TypeID", "dbo.RealEstateTypologies");
            DropForeignKey("dbo.Localizations", "RealEstateID", "dbo.RealEstates");
            DropIndex("dbo.Localizations", new[] { "RealEstateID" });
            DropIndex("dbo.RealEstates", new[] { "TypeID" });
            DropTable("dbo.Localizations");
            DropTable("dbo.RealEstates");
        }
    }
}
