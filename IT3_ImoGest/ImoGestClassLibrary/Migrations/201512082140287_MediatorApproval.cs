namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MediatorApproval : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Adverts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        TypeID = c.Int(nullable: false),
                        RealEstateID = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OwnerID = c.String(maxLength: 128),
                        DateCreation = c.DateTime(nullable: false),
                        DateLastUpdate = c.DateTime(nullable: false),
                        MediatorID = c.String(maxLength: 128),
                        MediatorValidation = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.MediatorID)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerID)
                .ForeignKey("dbo.RealEstates", t => t.RealEstateID, cascadeDelete: true)
                .ForeignKey("dbo.AdvertTypes", t => t.TypeID, cascadeDelete: true)
                .Index(t => t.TypeID)
                .Index(t => t.RealEstateID)
                .Index(t => t.OwnerID)
                .Index(t => t.MediatorID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.RealEstates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 120),
                        TypologyID = c.Int(nullable: false),
                        Area = c.Double(nullable: false),
                        OwnerID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerID)
                .ForeignKey("dbo.RealEstateTypologies", t => t.TypologyID, cascadeDelete: true)
                .Index(t => t.TypologyID)
                .Index(t => t.OwnerID);
            
            CreateTable(
                "dbo.Localizations",
                c => new
                    {
                        RealEstateID = c.Int(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.RealEstateID)
                .ForeignKey("dbo.RealEstates", t => t.RealEstateID, cascadeDelete: true)
                .Index(t => t.RealEstateID);
            
            CreateTable(
                "dbo.Photos",
                c => new
                    {
                        PhotoID = c.Int(nullable: false, identity: true),
                        PhotoName = c.String(maxLength: 255),
                        ContentType = c.String(maxLength: 100),
                        Content = c.Binary(),
                        RealEstateID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PhotoID)
                .ForeignKey("dbo.RealEstates", t => t.RealEstateID, cascadeDelete: true)
                .Index(t => t.RealEstateID);
            
            CreateTable(
                "dbo.RealEstateTypologies",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        ParentID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RealEstateTypologies", t => t.ParentID)
                .Index(t => t.ParentID);
            
            CreateTable(
                "dbo.AdvertTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AlertParameterFields",
                c => new
                    {
                        Field = c.Int(nullable: false),
                        Constraint = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Field, t.Constraint });
            
            CreateTable(
                "dbo.AlertParameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Field = c.Int(nullable: false),
                        Constraint = c.Int(nullable: false),
                        Value = c.String(nullable: false, maxLength: 100),
                        AlertID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Alerts", t => t.AlertID, cascadeDelete: true)
                .Index(t => t.AlertID);
            
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        Message = c.String(),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        OwnerID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerID)
                .Index(t => t.OwnerID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AlertParameters", "AlertID", "dbo.Alerts");
            DropForeignKey("dbo.Alerts", "OwnerID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Adverts", "TypeID", "dbo.AdvertTypes");
            DropForeignKey("dbo.Adverts", "RealEstateID", "dbo.RealEstates");
            DropForeignKey("dbo.RealEstates", "TypologyID", "dbo.RealEstateTypologies");
            DropForeignKey("dbo.RealEstateTypologies", "ParentID", "dbo.RealEstateTypologies");
            DropForeignKey("dbo.Photos", "RealEstateID", "dbo.RealEstates");
            DropForeignKey("dbo.RealEstates", "OwnerID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Localizations", "RealEstateID", "dbo.RealEstates");
            DropForeignKey("dbo.Adverts", "OwnerID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Adverts", "MediatorID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Alerts", new[] { "OwnerID" });
            DropIndex("dbo.AlertParameters", new[] { "AlertID" });
            DropIndex("dbo.RealEstateTypologies", new[] { "ParentID" });
            DropIndex("dbo.Photos", new[] { "RealEstateID" });
            DropIndex("dbo.Localizations", new[] { "RealEstateID" });
            DropIndex("dbo.RealEstates", new[] { "OwnerID" });
            DropIndex("dbo.RealEstates", new[] { "TypologyID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Adverts", new[] { "MediatorID" });
            DropIndex("dbo.Adverts", new[] { "OwnerID" });
            DropIndex("dbo.Adverts", new[] { "RealEstateID" });
            DropIndex("dbo.Adverts", new[] { "TypeID" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Alerts");
            DropTable("dbo.AlertParameters");
            DropTable("dbo.AlertParameterFields");
            DropTable("dbo.AdvertTypes");
            DropTable("dbo.RealEstateTypologies");
            DropTable("dbo.Photos");
            DropTable("dbo.Localizations");
            DropTable("dbo.RealEstates");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Adverts");
        }
    }
}
