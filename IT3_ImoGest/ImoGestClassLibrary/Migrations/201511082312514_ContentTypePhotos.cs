namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContentTypePhotos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Photos", "ContentType", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Photos", "ContentType");
        }
    }
}
