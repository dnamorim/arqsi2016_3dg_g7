// <auto-generated />
namespace ImoGest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddedOwnerIDToRealEstate : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedOwnerIDToRealEstate));
        
        string IMigrationMetadata.Id
        {
            get { return "201511090313247_AddedOwnerIDToRealEstate"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
