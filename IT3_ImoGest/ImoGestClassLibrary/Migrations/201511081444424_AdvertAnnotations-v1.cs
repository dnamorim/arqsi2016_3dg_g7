namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdvertAnnotationsv1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Adverts", name: "Type_ID", newName: "TypeID");
            RenameIndex(table: "dbo.Adverts", name: "IX_Type_ID", newName: "IX_TypeID");
            AlterColumn("dbo.Adverts", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Adverts", "Title", c => c.String());
            RenameIndex(table: "dbo.Adverts", name: "IX_TypeID", newName: "IX_Type_ID");
            RenameColumn(table: "dbo.Adverts", name: "TypeID", newName: "Type_ID");
        }
    }
}
