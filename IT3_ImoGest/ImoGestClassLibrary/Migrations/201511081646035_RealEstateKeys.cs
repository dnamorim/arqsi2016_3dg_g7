namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstateKeys : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.RealEstates", name: "Type_ID", newName: "TypeID");
            RenameIndex(table: "dbo.RealEstates", name: "IX_Type_ID", newName: "IX_TypeID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.RealEstates", name: "IX_TypeID", newName: "IX_Type_ID");
            RenameColumn(table: "dbo.RealEstates", name: "TypeID", newName: "Type_ID");
        }
    }
}
