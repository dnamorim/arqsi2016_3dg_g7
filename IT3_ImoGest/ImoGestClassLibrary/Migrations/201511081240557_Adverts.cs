namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Adverts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Adverts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Owner_Id = c.String(nullable: false, maxLength: 128),
                        Property_ID = c.Int(nullable: false),
                        Type_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_Id, cascadeDelete: true)
                .ForeignKey("dbo.RealEstates", t => t.Property_ID, cascadeDelete: true)
                .ForeignKey("dbo.AdvertTypes", t => t.Type_ID, cascadeDelete: true)
                .Index(t => t.Owner_Id)
                .Index(t => t.Property_ID)
                .Index(t => t.Type_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Adverts", "Type_ID", "dbo.AdvertTypes");
            DropForeignKey("dbo.Adverts", "Property_ID", "dbo.RealEstates");
            DropForeignKey("dbo.Adverts", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Adverts", new[] { "Type_ID" });
            DropIndex("dbo.Adverts", new[] { "Property_ID" });
            DropIndex("dbo.Adverts", new[] { "Owner_Id" });
            DropTable("dbo.Adverts");
        }
    }
}
