namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstateTypologyDataAnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RealEstateTypologies", "Name", c => c.String(nullable: false, maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RealEstateTypologies", "Name", c => c.String(nullable: false));
        }
    }
}
