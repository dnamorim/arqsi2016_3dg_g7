namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOwnerIDToRealEstate : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.RealEstates", name: "Owner_Id", newName: "OwnerID");
            RenameIndex(table: "dbo.RealEstates", name: "IX_Owner_Id", newName: "IX_OwnerID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.RealEstates", name: "IX_OwnerID", newName: "IX_Owner_Id");
            RenameColumn(table: "dbo.RealEstates", name: "OwnerID", newName: "Owner_Id");
        }
    }
}
