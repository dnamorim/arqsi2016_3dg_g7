namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CoordinatesFix : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Localizations", "Altitude");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Localizations", "Altitude", c => c.Double(nullable: false));
        }
    }
}
