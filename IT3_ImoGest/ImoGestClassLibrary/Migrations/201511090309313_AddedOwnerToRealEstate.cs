namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOwnerToRealEstate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RealEstates", "Owner_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.RealEstates", "Owner_Id");
            AddForeignKey("dbo.RealEstates", "Owner_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RealEstates", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.RealEstates", new[] { "Owner_Id" });
            DropColumn("dbo.RealEstates", "Owner_Id");
        }
    }
}
