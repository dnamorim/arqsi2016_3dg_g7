namespace ImoGestClassLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RealEstateFixing : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Adverts", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Adverts", new[] { "Owner_Id" });
            AlterColumn("dbo.Adverts", "Owner_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Adverts", "Owner_Id");
            AddForeignKey("dbo.Adverts", "Owner_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Adverts", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Adverts", new[] { "Owner_Id" });
            AlterColumn("dbo.Adverts", "Owner_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Adverts", "Owner_Id");
            AddForeignKey("dbo.Adverts", "Owner_Id", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
